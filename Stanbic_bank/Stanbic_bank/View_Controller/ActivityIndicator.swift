//
//  ActivityIndicator.swift
//  ALFAPARF MILANO
//
//  Created by vijay patidar on 10/12/18.
//  Copyright © 2018 5Exceptions. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class nvIndicator: NSObject {
    
    var actiVityIndicator : NVActivityIndicatorView? = nil
    var imgBlack : UIImageView? = nil
    
    //MARK: Add Remove Activity indicator view
    func showActivityIndicator(view : UIView) {
        
        self.removeActivityIndicator()
        
        imgBlack = UIImageView.init(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        imgBlack?.alpha = 0.5
        imgBlack?.backgroundColor = UIColor.black
        imgBlack?.isUserInteractionEnabled = true
        view.addSubview(imgBlack!)
        let centerXori = (view.frame.size.width / 2 - 40)
        let centerYori = (view.frame.size.height / 2 - 20)
        let frame = CGRect(x: centerXori, y: centerYori, width: 80, height: 40)
        
        actiVityIndicator = NVActivityIndicatorView(frame: frame, type: .ballPulseSync, color: UIColor.white, padding: nil)
        actiVityIndicator?.backgroundColor = UIColor.clear
        view.addSubview(actiVityIndicator!)
        actiVityIndicator?.startAnimating()
    }
    
    func removeActivityIndicator() {
        if actiVityIndicator != nil {
            imgBlack?.removeFromSuperview()
            actiVityIndicator?.stopAnimating()
            actiVityIndicator?.removeFromSuperview()
            actiVityIndicator = nil
        }
    }
    
}
