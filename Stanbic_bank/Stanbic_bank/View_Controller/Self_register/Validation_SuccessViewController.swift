
//
//  Validation_SuccessViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 11/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class Validation_SuccessViewController: UIViewController {
    
    @IBOutlet weak var lblTitle2: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        
        lblTitle2.text = "Your details have successfully been \nvalidated. Now, create a PIN that you will \nuse to login to your account "
   }
 
    // Mark:- Create A PIN-
    @IBAction func btn_Create_Pin(_ sender: UIButton) {
        
    }
    
    
    // Mark:- BACK-
    @IBAction func btn_Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
