


//
//  Make_ac_ExitViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 12/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class Make_ac_ExitViewController: UIViewController {
    
    struct QueData{
        let queKey: String
        let question: String
        let charLimit: String
        let dataType: String
    }
    
    struct AnsData{
        var answer: String
        let queKey: String
    }
    
    var ansArr = [AnsData]()
    var queDataArr = [QueData]()
    
    @IBOutlet weak var tableQueAns: UITableView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var btnSelectCountry: UIButton!
    @IBOutlet weak var viewOfTxtField: UIView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var txtMobileNo: UITextField!
    
    var date = ""
    let formatter = DateFormatter()
    
    
    var textarr = [TextFieldValidation]()
    var birthlimit:Int!
    var birthQuestion:String!
    var birthType:Int!
    
    var AccLimit:Int!
    var AccQuestion:String!
    var AccType:Int!
    
    var IdLimt:Int!
    var IdQuestion:String!
    var IdType:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.dateFormat = "dd/MM/yyyy"
        
        
        FetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    @IBAction func btnContinue(_ sender: UIButton) {
        print(self.ansArr)
        submitQueAns()
    }
    
    // Mark:- Countinue :-
    @IBAction func Btn_Countinue(_ sender: UIButton) {
        
        
        //      let activtyIndicator = nvIndicator()
        //      activtyIndicator.showActivityIndicator(view: self.view)
        
        //Call API for send activtion code to mobile no.
        
        
    }
    
    func submitQueAns(){
        
        var dict = [String: String]()
        
        for ans in ansArr{
            dict[ans.queKey] = ans.answer
        }
        
        RestAPIManager.sharedInstance.Fetch_Request(data: dict as AnyObject, mobileno: "\(txtMobileNo.text!)")
        { (json) in
            
            DispatchQueue.main.async {
                
                //Hide Indicator
                //   activtyIndicator.removeActivityIndicator()
                
                if json["STATUS_CODE"].intValue == 200 {
                    
                    print(json)
                    
                    let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "ValidationSuccess_VC") as! ValidationSuccess_VC
                    self.present(nextvc, animated: true, completion: nil)
                }
                else {
                    self.Alermsg(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                    
                    //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
                }
            }
        }

    }
    
    // Fetch Quetions :-
    func FetchData() {
        if isInternetAvailable() == false {
            
            self.showMessage(title: "Error", message: "No internet connection!")
            return
            
        }
        else {
            
                       let activtyIndicator = nvIndicator()
                        activtyIndicator.showActivityIndicator(view: self.view)
            
            //Call API for send activtion code to mobile no.
            RestAPIManager.sharedInstance.Fetch_Request(mobileno: txtMobileNo.text ?? "1234242424")
            { (json) in
                
                DispatchQueue.main.async {
                    
                    //Hide Indicator
                      activtyIndicator.removeActivityIndicator()
                    
                    if json["STATUS_CODE"].intValue == 200 {
                        
                        let dictnry = json["RESULTS_ARRAY"].dictionaryValue
                        
                        for (key , value) in dictnry{
                            let key = key
                            let questn = value.dictionaryValue["question"]?.string ?? ""
                            let charLimit = value.dictionaryValue["charlimit"]?.string ?? ""
                            let dataType = value.dictionaryValue["dataType"]?.string ?? ""
                            
                            let queTemp = QueData(queKey: key, question: questn, charLimit: charLimit, dataType: dataType)
                            self.queDataArr.append(queTemp)
                        }
                        self.ansArr = Array(repeating: AnsData(answer: "", queKey: ""), count: self.queDataArr.count)
                        self.tableQueAns.reloadData()
                    }
                    else {
                        self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                        
                        //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
                    }
                }
            }
        }
    }
    
    
    
    
    func Alermsg(title : String, message : String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
            
        })
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK:-  UITextFieldDelegate
extension Make_ac_ExitViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
}

//MARK:- UITableViewDataSource and Deleget
extension Make_ac_ExitViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return queDataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QueAnsTableCell", for: indexPath) as! QueAnsTableCell
        let tempData = queDataArr[indexPath.row]
        cell.txtQueAns.tag = indexPath.row
        cell.txtQueAns.addUI(placeholder: tempData.question)
        cell.charLimit = Int(tempData.charLimit) ?? 0
        cell.delegate = self
        if tempData.dataType == "1"{
            
            
        }else if tempData.dataType == "2"{
        
            
        }else if tempData.dataType == "3"{
            cell.txtQueAns.keyboardType = .numberPad
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

extension Make_ac_ExitViewController: AddTextData{
    func getText(textField: UITextField) {
        self.ansArr[textField.tag].answer = textField.text ?? ""
    }
    
    
}

/*
 
 // Struct for Question data response
 
 
 
 
 var ansKeyArr = [AnsData]()
 var queArr = [QueData]()
 
 var arr_question = [JSON]()
 let formatter = DateFormatter()
 var date = ""
 
 var indexDatefiled = 99
 var dateselect = false
 
 var arrdata = NSMutableArray()
 
 override func viewDidLoad() {
 super.viewDidLoad()
 formatter.dateFormat = "dd/MM/yyyy"
 
 
 NotificationCenter.default.addObserver(self, selector: #selector(Mobile_numberViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
 NotificationCenter.default.addObserver(self, selector: #selector(Mobile_numberViewController.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
 
 FetchData()
 
 }
 
 
 
 
 // MARK:_ keyboardWillShow
 @objc func keyboardWillShow(_ notification: Notification) {
 if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
 let keyboardRectangle = keyboardFrame.cgRectValue
 let keyboardHeight = keyboardRectangle.height
 print(keyboardHeight)
 
 
 Const_btm_Tableview.constant = keyboardHeight - 10
 UIView.animate(withDuration: 0.5) {
 self.view.layoutIfNeeded()
 }
 
 }
 }
 // MARK:_ keyboardWillHide
 @objc func keyboardWillHide(_ notification: Notification) {
 Const_btm_Tableview.constant = 0
 UIView.animate(withDuration: 0.5) {
 self.view.layoutIfNeeded()
 }
 }
 
 
 //MARK:_ UITABLEVIEW DATASOURCE AND DELEGATES:-
 func numberOfSections(in tableView: UITableView) -> Int {
 return 2
 }
 
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 if section == 0 {
 return queArr.count
 }
 else{
 return 1
 }
 }
 
 func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 if indexPath.section == 0 {
 let Cell  = tableView.dequeueReusableCell(withIdentifier: "textfield_cell", for: indexPath) as! RegistraionCell
 
 
 
 let dict = (queArr[indexPath.row].dictQue)
 let str = dict["question"]?.string
 Cell.baseView.layer.cornerRadius = 4
 Cell.baseView.layer.borderWidth = 1
 Cell.baseView.layer.borderColor = UIColor.lightGray.cgColor
 Cell.txt_Title.addUI(placeholder:str!)
 if indexDatefiled == indexPath.row {
 Cell.txt_Title.text = date
 }
 Cell.txt_Title.delegate   = Cell
 Cell.txt_Title.addTarget(self, action: #selector(textFieldDidChange(_ : )), for: UIControl.Event.editingChanged)
 Cell.txt_Title.tag = indexPath.row
 Cell.txt_type = (dict["dataType"])!.int!
 if (dict["charlimit"])!.int! == 0 {
 Cell.limit =  100
 }
 else{
 Cell.limit =  (dict["charlimit"])!.int!
 }
 Cell.delegate = self
 
 //            if indexPath.row == indx{
 //                cell.txtTitle.text = date
 //            }
 
 
 
 return Cell
 }
 else{
 let Cell  = tableView.dequeueReusableCell(withIdentifier: "button_cell", for: indexPath) as! RegistraionCell
 Cell.delegate = self
 return Cell
 }
 }
 
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
 if indexPath.section  == 0 {
 return 70
 }
 else{
 return 60
 }
 }
 
 // Mark:- Open Date PIcker -
 func Open_Datepicker(cell: RegistraionCell) {
 
 let indexpath = Tbl_Registration.indexPath(for: cell)
 indexDatefiled = indexpath!.row
 Date_pickerview.isHidden = false
 }
 
 @objc func textFieldDidChange( _ textField: UITextField){
 let key = queArr[textField.tag].key
 let answer = textField.text ?? ""
 ansKeyArr.append(AnsData(key: key, ans: answer))
 
 }
 
 // Mark:- Continue btn-
 func COUNTINUE_TAP(cell: RegistraionCell) {
 
 print(ansKeyArr)
 
 }
 
 func SubmitData(dict :NSDictionary) {
 
 if isInternetAvailable() == false {
 
 self.showMessage(title: "Error", message: "No internet connection!")
 return
 
 }
 else {
 
 //            let activtyIndicator = nvIndicator()
 //            activtyIndicator.showActivityIndicator(view: self.view)
 
 //Call API for send activtion code to mobile no.
 RestAPIManager.sharedInstance.Fetch_Request(mobileno: "700707453")
 { (json) in
 
 DispatchQueue.main.async {
 
 //Hide Indicator
 //   activtyIndicator.removeActivityIndicator()
 
 if json["STATUS_CODE"].intValue == 200 {
 
 print(json)
 
 // let dict  = (json["RESULTS_ARRAY"].dictionaryValue)
 
 
 
 for (kind, numbers) in json["RESULTS_ARRAY"].dictionaryValue {
 print(numbers)
 
 let dict = [kind:numbers]
 
 self.arrdata.add(dict)
 self.arr_question.append(numbers)
 
 
 
 }
 
 self.Tbl_Registration.reloadData()
 
 }
 else {
 self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
 
 //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
 }
 }
 }
 }
 
 }
 
 // Fetch Quetions :-
 func FetchData() {
 if isInternetAvailable() == false {
 
 self.showMessage(title: "Error", message: "No internet connection!")
 return
 
 }
 else {
 
 //            let activtyIndicator = nvIndicator()
 //            activtyIndicator.showActivityIndicator(view: self.view)
 
 //Call API for send activtion code to mobile no.
 RestAPIManager.sharedInstance.Fetch_Request(mobileno: "700707453")
 { (json) in
 
 DispatchQueue.main.async {
 
 //Hide Indicator
 //   activtyIndicator.removeActivityIndicator()
 
 if json["STATUS_CODE"].intValue == 200 {
 
 print(json)
 
 
 
 
 for (kind, numbers) in json["RESULTS_ARRAY"].dictionaryValue {
 print(numbers)
 self.arr_question.append(numbers)
 
 let queTemp = QueData(key: kind, dictQue: numbers.dictionaryValue)
 print(queTemp)
 self.queArr.append(queTemp)
 
 }
 
 
 self.ansKeyArr = Array(repeating: AnsData(key: "", ans: ""), count: self.queArr.count)
 self.Tbl_Registration.reloadData()
 
 }
 else {
 self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
 
 //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
 }
 }
 }
 }
 }
 
 
 
 
 // MARK:_Check valid mobile no.
 func checkValidMobileNo() -> Bool {
 
 if self.txt_mob.text?.count == 10 {
 
 let prefix = String((self.txt_mob.text?.prefix(2))!)
 if prefix != "07" {
 self.txt_mob.showError(errStr: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
 //    imgLineTxtField.backgroundColor = .red
 //showAlert(message: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
 return false
 }
 return true
 
 }
 else if self.txt_mob.text?.count == 9 {
 let prefix = String((self.txt_mob.text?.prefix(1))!)
 if prefix == "0" {
 
 return false
 
 }
 else if prefix != "7" {
 self.txt_mob.showError(errStr: "Invalid mobile number. Enter 9 digit mobile number in the format 7XXX.")
 imgLineTxtField.backgroundColor = .red
 //showAlert(message: "Invalid mobile number. Enter 9 digit mobile number in the format 7XXX.")
 return false
 }
 
 return true
 }
 else {
 // self.txtMobNo.showError(errStr: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
 // imgLineTxtField.backgroundColor = .red
 //showAlert(message: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
 return false
 }
 
 }
 }
 
 // MARK:_ UITableViewCell - Class
 class RegistraionCell: UITableViewCell, UITextFieldDelegate {
 
 
 @IBOutlet weak var baseView: UIView!
 @IBOutlet weak var txt_Title: UITextField!
 
 var limit = 250
 var txt_type = 1
 
 var delegate:CountinueDelegate?
 
 override func awakeFromNib() {
 super.awakeFromNib()
 
 }
 
 func textFieldDidBeginEditing(_ textField: UITextField) {
 textField.didBegin()
 }
 
 func textFieldDidEndEditing(_ textField: UITextField) {
 textField.didEnd()
 
 }
 
 func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
 
 let currentString: NSString = textField.text! as NSString
 let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
 
 if txt_type == 1 {
 return newString.length <= self.limit
 }
 else if txt_type == 2 {
 //  textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
 //            textField.addInputViewDatePicker(target: self, selector: doneButtonPressed(textfield: textField))
 return newString.length <= self.limit
 }
 else{
 textField.keyboardType = .numberPad
 return newString.length <= self.limit
 }
 }
 
 //    @objc func doneButtonPressed(textfield:UITextField) {
 //        if let  datePicker = self.textField.inputView as? UIDatePicker {
 //            let dateFormatter = DateFormatter()
 //            dateFormatter.dateStyle = .medium
 //            self.text.text = dateFormatter.string(from: datePicker.date)
 //        }
 //        self.resignFirstResponder()
 //    }
 
 //    @objc func textFieldDidChange(_ textField:UITextField) {
 //          delegate?.Open_Datepicker(cell: self)
 //    }
 
 @IBAction func BtN_COUNTIUE(_ sender: UIButton) {
 var valueArr = [String]()
 
 delegate?.COUNTINUE_TAP(cell: self)
 }
 }
 
 
 
 */
