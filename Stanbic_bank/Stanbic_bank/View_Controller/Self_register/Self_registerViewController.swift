//
//  Self_registerViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 11/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class Self_registerViewController: UIViewController {
    
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var lblTitle2: UILabel!
    @IBOutlet weak var btn_newaccount: UIButton!
    var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        lblTitle2.text = "We Found an account with your number but it’s\n not registered for mobile banking. You can either \n register it or create and new account"
        btn_newaccount.layer.borderWidth = 0.5
        btn_newaccount.layer.borderColor = UIColor.green.cgColor
    
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Register", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    
    // Mark:- BACK-
    @IBAction func btn_Back(_ sender: UIButton) {
        dismiss(animated: true
            , completion: nil)
    }
    
    // Mark:- Register ac-
    @IBAction func btn_Register(_ sender: UIButton) {
//        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "Make_ac_ExitViewController") as! Make_ac_ExitViewController
        let nextVc = Make_ac_ExitViewController.getVCInstance()
        self.present(nextVc, animated: true, completion: nil)
    }
    
    // Mark:- Create ac-
    @IBAction func btn_Createac(_ sender: UIButton) {
        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "NewUserSteps_VC") as! NewUserSteps_VC
        nextvc.mobileNumber = mobileNumber
        self.present(nextvc, animated: true, completion: nil)
    }
   
}
