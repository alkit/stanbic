//
//  PopupVC.swift
//  Stanbic_bank
//
//  Created by Admin on 15/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit


protocol PopupVCDelegate: class{
    func getIndex(indx: Int)
}

class PopupVC: UIViewController {

    var valueArr = [String]()
    
    weak var delegate: PopupVCDelegate!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let popupVC = PopupVC.getVCInstance() as! PopupVC
//        popupVC.valueArr = ["sdsdfs"] // string array
//        popupVC.modalPresentationStyle = UIModalPresentationStyle.popover
//        popupVC.preferredContentSize = CGSize(width: 200, height: 87)
//        popupVC.popoverPresentationController?.delegate = self
//        
//        self.present(popupVC, animated: true)
//        
//        
//        let popover = popupVC.popoverPresentationController
//        popover!.sourceView = button
//        popover!.sourceRect = button.bounds
        
        //extension vc: UIPopoverPresentationControllerDelegate{
        //    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
        //    {
        //        return UIModalPresentationStyle.none
        //    }
        //}
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }

}
extension PopupVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(valueArr.count == 0)
        {
            self.tableView.frame = CGRect(x: tableView.frame.origin.x, y: self.tableView.frame.origin.y, width: 0, height: 0)
        }
        else if(valueArr.count == 1)
        {
            self.tableView.frame = CGRect(x: tableView.frame.origin.x, y: self.tableView.frame.origin.y, width: self.tableView.frame.width, height: 20)
        }
        
        return valueArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopupCell", for: indexPath)
        cell.textLabel?.text = valueArr[indexPath.row]
        
        return  cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(valueArr[indexPath.row])
        delegate.getIndex(indx: indexPath.row)
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
