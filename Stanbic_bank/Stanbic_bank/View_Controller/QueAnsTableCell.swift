//
//  QueAnsTableCell.swift
//  Stanbic_bank
//
//  Created by Admin on 15/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

protocol AddTextData {
    func getText(textField: UITextField)
}

class QueAnsTableCell: UITableViewCell {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var txtQueAns: UITextField!
    
    var charLimit = 0
    var delegate: AddTextData!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        txtQueAns.delegate = self
        txtQueAns.addTarget(self, action: #selector(textFieldChange), for: .editingChanged)
        
        baseView.layer.cornerRadius = 4
        baseView.layer.borderWidth = 1
        baseView.layer.borderColor = UIColor.lightGray.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension QueAnsTableCell: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func textFieldChange(_ textField: UITextField){
        delegate.getText(textField: textField)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.charLimit == 0{
            return true
        }else{
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= charLimit
        }
    }
    
}
