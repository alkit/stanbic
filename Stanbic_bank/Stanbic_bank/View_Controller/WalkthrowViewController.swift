//
//  WalkthrowViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 10/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit


class WalkThroughCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var lblTitle2: UILabel!
    @IBOutlet weak var lblTitle3: UILabel!
    @IBOutlet weak var consBottomTitle: NSLayoutConstraint!
}

class WalkthrowViewController: UIViewController{
    
    @IBOutlet weak var walkThroughCollView: UICollectionView!
    @IBOutlet var btn1: UIButton!
    @IBOutlet var btn2: UIButton!
    @IBOutlet var btn3: UIButton!
    
    
    //    @IBOutlet var pageControlBtns: [UIButton]!
    //    @IBOutlet weak var consBtnWidth: NSLayoutConstraint!
    //    @IBOutlet weak var bottomOfPageControl: NSLayoutConstraint!
    //    @IBOutlet weak var bottomOfGetStarted: NSLayoutConstraint!
    @IBOutlet weak var buttonGetStarted: UIButton!
    
    let walkImages = [ #imageLiteral(resourceName: "intro1"), #imageLiteral(resourceName: "intro 2"),#imageLiteral(resourceName: "intro3")]
    var selectedIndx = 0
    

    
        var arr_title = [
            [
                "first": "Making",
                "second": "banking easy",
                "last": "Wherever you are",
                ],
    [
                "first": "Amazing payment",
                "second": "exprience",
                "last": "Just tap and go",
                ],
    
    [
                "first": "Transact any time",
                "second": "anywere",
                "last": "let's go get started",
                ],
        ]
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonGetStarted.layer.cornerRadius = buttonGetStarted.frame.height/2
        btn1.layer.cornerRadius = 2
        btn2.layer.cornerRadius = 2
        btn3.layer.cornerRadius = 2
        btn1.backgroundColor = UIColor.white
        btn2.backgroundColor = UIColor.lightGray
        btn3.backgroundColor = UIColor.lightGray
        
        
          Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = walkThroughCollView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < walkImages.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    self.selectedIndx = (indexPath1?.row)!
                    self.setPageControl()
                }
                else {
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                    self.selectedIndx = (indexPath1?.row)!
                    self.setPageControl()
                }
            }
        }
    }
    
    func setPageControl(){

         if self.selectedIndx == 0 {
             btn1.backgroundColor = UIColor.white
            btn2.backgroundColor = UIColor.lightGray
            btn3.backgroundColor = UIColor.lightGray
        }
        if self.selectedIndx == 1 {
            btn2.backgroundColor = UIColor.white
            btn1.backgroundColor = UIColor.lightGray
            btn3.backgroundColor = UIColor.lightGray
            
        }
        if self.selectedIndx == 2 {
            btn1.backgroundColor = UIColor.lightGray
            btn3.backgroundColor = UIColor.white
           btn2.backgroundColor = UIColor.lightGray
        }
    }
    
    
    
    
    // Mark:-GET START-
    @IBAction func btn_GetStart(_ sender: UIButton) {
        let nextvc = storyboard?.instantiateViewController(withIdentifier: "Mobile_numberViewController") as! Mobile_numberViewController
        present(nextvc, animated: true, completion: nil)
    }

}

extension WalkthrowViewController: UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    // MARK:_ collectionView  DATASOURCE AND DELEGATE:_
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_title.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WalkThroughCell", for: indexPath) as! WalkThroughCell
        
        let dict = arr_title[indexPath.row]  as NSDictionary
        
        cell.imgView.image = walkImages[indexPath.row]
        
        cell.lblTitle1.text = dict.value(forKey: "first") as? String
        cell.lblTitle2.text = dict.value(forKey: "second") as? String
        cell.lblTitle3.text = dict.value(forKey: "last") as? String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return walkThroughCollView.frame.size
    }
    
}
