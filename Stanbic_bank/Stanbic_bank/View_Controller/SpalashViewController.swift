//
//  SpalashViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 10/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SpalashViewController: UIViewController {
    
    @IBOutlet weak var lbl_title_top: UILabel!
    @IBOutlet weak var lb_title_btm: UILabel!
    @IBOutlet weak var imgloader: UIImageView!

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_title_top.text = "Welcome to Stanbic \nMobile Banking"
        lb_title_btm.text = "Making sure your connection \nis secure"
        rotate()
       self.perform(#selector(SpalashViewController.performAction), with: nil, afterDelay: 2.0)
    }
    
    @objc func performAction() {
     stopRotating()
        
        let next = storyboard?.instantiateViewController(withIdentifier: "WalkthrowViewController") as! WalkthrowViewController
        present(next, animated: true, completion: nil)
    }
    

    // MARK:_ ROTATE 360 degree-
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        imgloader.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    // MARK:_ Stop Rotation-
    func stopRotating() {
        if  imgloader.layer.animation(forKey:"rotationAnimation") != nil {
             imgloader.layer.removeAnimation(forKey: "rotationAnimation")
        }
    }
    
}
