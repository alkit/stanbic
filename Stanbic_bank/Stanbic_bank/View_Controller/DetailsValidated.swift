//
//  DetailsValidated.swift
//  Stanbic_bank
//
//  Created by Admin on 15/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class DetailsValidated: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Register", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }

}
