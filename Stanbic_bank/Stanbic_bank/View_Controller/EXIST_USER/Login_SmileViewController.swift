//
//  Login_SmileViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 11/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit
import LocalAuthentication



class Login_SmileViewController: UIViewController{
    
    
    
    
    @IBOutlet weak var buttonLinkFingerPrint: UIButton!
    @IBOutlet weak var buttonNotNow: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    var mobileNo = ""
    var pin = ""
    


    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setFont()
        let context = LAContext()
        context.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
        self.checkUserDeviceSupport()
    }
 
    //BUTTON LINKED TO FINGER CLICKED
    @IBAction func buttonActionLinkFingerPrint(_ sender: UIButton){
        
        print("hello there!.. You have clicked the touch ID")
        
        let myContext = LAContext()
        let myLocalizedReasonString = "Biometric Authntication testing !! "
        
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    
                    DispatchQueue.main.async {
                        if success {
                            // User authenticated successfully, take appropriate action
                            
                            let alert = UIAlertController(title: "", message: "Awesome!!... User authenticated successfully", preferredStyle: .alert)
                           
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel) { action in
                                
                                let nextvc = FaceIDSuccessViewController.getVCInstance()
                                self.present(nextvc, animated: true, completion: nil)
                            })
                            self.present(alert, animated: true)
                       
                        } else {
                            // User did not authenticate successfully, look at error and take appropriate action
                            self.showMessage(title: "", message: "Sorry!!... User did not authenticate successfully")
                           // self.successLabel.text = "Sorry!!... User did not authenticate successfully"
                        }
                    }
                }
            } else {
              //
                //self.showMessage(title: "", message: "Sorry!!.. Could not evaluate policy.")
                // Could not evaluate policy; look at authError and present an appropriate message to user
                //successLabel.text = "Sorry!!.. Could not evaluate policy."
                self.fingerID()
            }
        } else {
            // Fallback on earlier versions
            self.showMessage(title: "", message: "Ooops!!.. This feature is not supported.")
            
        
           // successLabel.text = "Ooops!!.. This feature is not supported."
        }
        
        
    }

    
    func fingerID()
    {
             let encrPIN = encryptPIN(text: self.pin)
        
                //CHECK USER'S FINGERPRINT OR FACE ID
                let context = LAContext()
                Authntication(context).authnticateUser { (success, message) in
        
                    if success == true {
        
                        //SAVE USERNAME PASSWORD
                      if  Authntication().saveUserCredientiacial(username: self.mobileNo, password: encrPIN, isAuth: true) == true {
        
                            DispatchQueue.main.async {
                                let nextvc = FaceIDSuccessViewController.getVCInstance()
                                self.present(nextvc, animated: true, completion: nil)
                            }
                       }
                    }
                    else {
                        if message != "" {
                            self.showMessage(title: "", message: message)
                        }
        
                    }
                }
            }
    
    
    //BUTTON NOT NOW CLICKED
    @IBAction func btnNotNowClicked(_ sender: UIButton) {
    
        let nextvc = FaceIDSuccessViewController.getVCInstance()
        self.present(nextvc, animated: true, completion: nil)
    
    }
    
    func setFont() {
        
        
//        Fonts().set(object: lblLinkYourFingerprintToyourMobilebankingapp, fontType: 1, fontSize: 20, color: colorConstant.kBlackFont, title: "Link_your_fingerprint", placeHolder: "")
//
//        Fonts().set(object: lblUseFingerprintForFasterEasierAccess, fontType: 0, fontSize: 14, color: colorConstant.kBlackFont, title: "Use_your_fingerprint", placeHolder: "")
        
      //  Fonts().set(object: buttonNotNow, fontType: 1, fontSize: 14, color: colorConstant.kRedColor, title: "Notnow", placeHolder: "")
        
     //   Fonts().set(object: buttonLinkFingerPrint, fontType: 1, fontSize: 14, color: UIColor.white, title: "Link_FingerPrint", placeHolder: "")
        
    }
    
    
    //ENCRYPT PIN
    func encryptPIN(text : String) -> String {
        
        let input:String = text
        let cipher:String = CryptoHelper.encrypt(input:input) ?? "";
        //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
        //debugPrint("cipher:" + cipher);
        //let output:String = CryptoHelper.decrypt(input:cipher)!;
        return cipher
    }
    
    
    func checkUserDeviceSupport()
    {
        //IF USER DEVICE SUPPORT FACE ID, CHANGE LABEL ON SCREEN
        let context = LAContext()
        if Authntication(context).checkUserDeviceFaceIDSupport() == true
        {
            
         //   Fonts().set(object: buttonLinkFingerPrint, fontType: 3, fontSize: 15, color: UIColor.white, title: "Link_FACE_ID", placeHolder: "")
            
            imgView.image = UIImage.init(named: "Face ID.png")
          //  imgView.image = #imageLiteral(resourceName: "Face ID.png")
        }
    }

}
