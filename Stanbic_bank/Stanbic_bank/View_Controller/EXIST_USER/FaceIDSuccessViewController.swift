



//
//  FaceIDSuccessViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 12/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit
import LocalAuthentication
import Lottie

class FaceIDSuccessViewController: UIViewController {

    let context = LAContext()
    var mobileNo = ""
    var pin = ""
    @IBOutlet weak var successfulLogin: UILabel!
    @IBOutlet weak var btnDone: UIButton!
//    @IBOutlet weak var consBtnBottom: NSLayoutConstraint!
//    @IBOutlet weak var consBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var viewAnimation: LOTAnimationView!
    @IBOutlet weak var lblsubtitle: UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      //  self.consBtnWidth.constant = self.view.frame.size.width * 0.9
        self.btnDone.layer.cornerRadius = 8
        
        setFont()
        checkUserDeviceSupport()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
      //  let animatedView = LOTAnimationView(name: "Check Mark Success Data")
     //   animatedView.frame =  CGRect(x: -25, y: -25, width: (self.viewAnimation.frame.size.width) + 50, height: (self.viewAnimation.frame.size.height) + 50)
    //    self.viewAnimation.addSubview(animatedView)
     
    }
    
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Main", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    func setFont()
    {
      //  Fonts().set(object: successfulLogin, fontType: 3, fontSize: 18, color: colorConstant.kBlackColor, title : "successfulLogin", placeHolder: "")
        
        
     //   Fonts().set(object: btnDone, fontType: 3, fontSize: 15, color: colorConstant.kWhiteColor, title : "done_1", placeHolder: "")
        
     //   Fonts().set(object: lblsubtitle, fontType: 0, fontSize: 14, color: colorConstant.kBlackColor, title : "you_can_now_touch_id", placeHolder: "")
    }
    
    
    func checkUserDeviceSupport()
    {
        //IF USER DEVICE SUPPORT FACE ID, CHANGE LABEL ON SCREEN
        if Authntication(context).checkUserDeviceFaceIDSupport() == true
        {
         //   Fonts().set(object: successfulLogin, fontType: 3, fontSize: 18, color: colorConstant.kBlackFont, title : "FaceId_Enabled", placeHolder: "")
            
         //   Fonts().set(object: lblsubtitle, fontType: 0, fontSize: 14, color: colorConstant.kBlackFont, title : "you_can_now_face_id", placeHolder: "")
        }
        else {
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Button done clicked
    @IBAction func btnDoneClicked(_ sender: UIButton) {
        
        
     }
}

