//
//  Reset_PinViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 11/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class PinViewController: UIViewController{
    
    @IBOutlet weak var lbl_title_top: UILabel!
    @IBOutlet weak var btn_otp1: UIButton!
    @IBOutlet weak var btn_otp2: UIButton!
    @IBOutlet weak var btn_otp3: UIButton!
    @IBOutlet weak var btn_otp4: UIButton!
  
    @IBOutlet weak var keyBoardHeight: NSLayoutConstraint!
    
    var pin = ""
    var name = ""
    var mob = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if name == "" {
            name  = "James"
        }
        
        btn_otp1.layer.cornerRadius = btn_otp1.frame.height / 2
        btn_otp2.layer.cornerRadius = btn_otp1.frame.height / 2
        btn_otp3.layer.cornerRadius = btn_otp1.frame.height / 2
        btn_otp4.layer.cornerRadius = btn_otp1.frame.height / 2
        lbl_title_top.text = "WelCome back, \(name)!\n please enter  your PIN to login"
        
        keyBoardHeight.constant = (self.view.frame.width * 372.0)/375.0
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        // enteredOtp = otpString
        print("OTPString: \(otpString)")
    }
    
    // Mark:- FOrGot PIN-
    @IBAction func btn_ForgotPIN(_ sender: UIButton) {
        print(pin)
        
    }
    
    // Mark:- BACK PIN-
    @IBAction func btn_BACk(_ sender: UIButton) {
        if btn_otp4.currentTitle != nil{
            btn_otp4.setTitle(nil, for: .normal)
            pin.removeLast()
            return
        }
        if btn_otp3.currentTitle != nil{
            btn_otp3.setTitle(nil, for: .normal)
            pin.removeLast()
            return
        }
        if btn_otp2.currentTitle != nil{
            btn_otp2.setTitle(nil, for: .normal)
            pin.removeLast()
            return
        }
        if btn_otp1.currentTitle != nil{
            btn_otp1.setTitle(nil, for: .normal)
            pin.removeLast()
            return
        }
    }
    
    // Mark:- Enter Digit Keyboard PIN-
    @IBAction func btn_EnterDIgit(_ sender: UIButton) {
        let str = "\(sender.tag)"
        
        if btn_otp1.currentTitle == nil{
            btn_otp1.setTitle(str, for: .normal)
            pin = "\(pin)\(str)"
            return
        }
        if btn_otp2.currentTitle == nil{
            btn_otp2.setTitle(str, for: .normal)
              pin = "\(pin)\(str)"
            return
        }
        if btn_otp3.currentTitle == nil{
            btn_otp3.setTitle(str, for: .normal)
              pin = "\(pin)\(str)"
            return
        }
        if btn_otp4.currentTitle == nil{
            btn_otp4.setTitle(str, for: .normal)
              pin = "\(pin)\(str)"
           GenratePin(pin: pin)
            
            return
        }
    }
    
    
    func GenratePin(pin:String) {
     
        //            let activtyIndicator = nvIndicator()
        //            activtyIndicator.showActivityIndicator(view: self.view)
        
        //Call API for Reset Pin.
        RestAPIManager.sharedInstance.Validate_Pin_Request(mobileno: "254\(mob)", pin: pin)
        { (json) in
            
            print(json)
            DispatchQueue.main.async {
                
                //Hide Indicator
                //   activtyIndicator.removeActivityIndicator()
                
                if json["STATUS_CODE"].intValue == 200 {
                    
                    print(json)
//                    {
//                        "STATUS_MESSAGE" : "Successfully confirmed activation key. Please enjoy our services",
//                        "RESULT_ARRAY" : {
//                            "NAME" : "",
//                            "PROFILE_ID" : "",
//                            "LAST_LOGIN" : "",
//                            "MB_STATUS" : 0,
//                            "PIN_STATUS" : ""
//                        },
//                        "STATUS_CODE" : 200
//                    }
                    
                    let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "Login_SmileViewController") as! Login_SmileViewController
                    nextvc.pin = pin
                    nextvc.mobileNo = self.mob
                   self.present(nextvc, animated: true, completion: nil)
                    
                }
                else {
                    self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                    //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
                }
            }
        }
    }
}

