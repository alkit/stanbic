//
//  OtpViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 11/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit
import KWVerificationCodeView

class OtpViewController: UIViewController,KWVerificationCodeViewDelegate {
  
    @IBOutlet weak var lbl_title_top: UILabel!
    @IBOutlet weak var lbl_title_btm: UILabel!
    @IBOutlet weak var btn_resendCode: UIButton!
    @IBOutlet weak var otpView: KWVerificationCodeView!
    
    @IBOutlet weak var View_loader: UIView!
    @IBOutlet weak var Img_loader: UIImageView!
    let activtyIndicator = nvIndicator()
    
    var otp = ""
     var mob = ""
    
    var countdownTimer: Timer!
    var totalTime = 30

    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        lbl_title_top.text = "Please wait while we verify \nyour mobile number"
        btn_resendCode.isEnabled = false
        otpView.delegate = self
        otpView.keyboardType = .numberPad
         startTimer()
        
        
          Validate_Otp()
    }
    override func viewWillAppear(_ animated: Bool) {
           otpView.isHidden = true
        
        super.viewWillAppear(true)
        
    }
    
    
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        lbl_title_btm.text = "Resend Code in \(timeFormatted(totalTime))"
         if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        btn_resendCode.isEnabled = true
        countdownTimer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    // OTP VIEW
    func didChangeVerificationCode() {
         if otpView.hasValidCode()
        {
            otpView.resignFirstResponder()
            
       }
    }
    
    // Mark:- BACK-
    @IBAction func btn_Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
       // Mark:- Btn Resend COde-
    @IBAction func btn_Resend_otp(_ sender: UIButton) {
        if isInternetAvailable() == false {
            
            self.showMessage(title: "Error", message: "No internet connection!")
            return
            
        }
        else {
            
            activtyIndicator.showActivityIndicator(view: self.view)
            //            let activtyIndicator = nvIndicator()
            //            activtyIndicator.showActivityIndicator(view: self.view)
            
            //Call API for send activtion code to mobile no.
    RestAPIManager.sharedInstance.get_validation_key(mobileno: ("254\(mob)"))
            { (json) in
                
                DispatchQueue.main.async {
                    
                    //Hide Indicator
                    self.activtyIndicator.removeActivityIndicator()
                    
                    if json["STATUS_CODE"].intValue == 200 {
                        
                        print(json)
                        
                          }
                    else {
                        self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                        
                        //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
                    }
                }
            }
        }

  }
  
    
     //  Otp validation
    func Validate_Otp()  {
      
        if isInternetAvailable() == false {
            
            self.showMessage(title: "Error", message: "No internet connection!")
            return
            
        }
        else {
            
            otpView.isHidden = false
            self.rotate()
            
            //            let activtyIndicator = nvIndicator()
            //            activtyIndicator.showActivityIndicator(view: self.view)
            
            activtyIndicator.showActivityIndicator(view: self.view)
            //Call API for send activtion code to mobile no.
           RestAPIManager.sharedInstance.validation_activation_pin(mobileno: mob, activation_code: otp) { (json) in
                DispatchQueue.main.async {
                    
                    self.stopRotating()
                    //Hide Indicator
                       self.activtyIndicator.removeActivityIndicator()
                    let jsonValue = json["STATUS_CODE"].intValue
                    if jsonValue == 200 {
                        
                        print(json)
//                        {
//                            "STATUS_MESSAGE" : "Successfully confirmed activation key. Please enjoy our services",
//                            "STATUS_CODE" : 200,
//                            "RESULT_ARRAY" : {
//                                "NAME" : "",
//                                "LAST_LOGIN" : "",
//                                "PROFILE_ID" : "",
//                                "MB_STATUS" : 0,
//                                "PIN_STATUS" : ""
//                            }
//                        }
                        
                        if json["RESULT_ARRAY"]["MB_STATUS"] == 0 // 0
                        {
                            // Self_registerViewController
                            
                          let value =  Self_registerViewController.getVCInstance() as! Self_registerViewController
                           value.mobileNumber = self.mob
                            self.present(value, animated: true, completion: nil)
                            
                        }else
                        {
                            let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "PinViewController") as! PinViewController
                            nextvc.name =  json["NAME"].stringValue
                            nextvc.mob =  self.mob
                            self.present(nextvc, animated: true, completion: nil)
                        }
                        // exist user
                      //  if json["STATUS_MESSAGE"].stringValue == "1"{
                        
                     //   }
                            // New user
                      //  else{
//                            let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "Reset_PinViewController") as! Reset_PinViewController
//                            self.present(nextvc, animated: true, completion: nil)
                     //   }
                        
                      }
                    else {
                        self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                        
                        //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
                    }
                }
            }
        }

    }
    
    
    // MARK:_ ROTATE 360 degree-
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        Img_loader.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    // MARK:_ Stop Rotation-
    func stopRotating() {
        if  Img_loader.layer.animation(forKey:"rotationAnimation") != nil {
            Img_loader.layer.removeAnimation(forKey: "rotationAnimation")
        }
    }
    
}
