//
//  MobileViewController.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 10/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class Mobile_numberViewController: UIViewController,UITextFieldDelegate {
    
        @IBOutlet weak var lbl_title_top: UILabel!
        @IBOutlet weak var lbl_title_btm: UILabel!
        @IBOutlet weak var txt_mob: UITextField!
        @IBOutlet weak var lblContryCode: UILabel!
       @IBOutlet weak var imgLineTxtField: UIImageView!
       @IBOutlet weak var View_box: UIView!
       @IBOutlet weak var btm_button_constant: NSLayoutConstraint!
    @IBOutlet weak var btnCountry: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    var const = NSLayoutConstraint()
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        txt_mob.becomeFirstResponder()
         lbl_title_top.text = "Please Enter your mobile \nnumber below"
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(Mobile_numberViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(Mobile_numberViewController.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
          const = btm_button_constant
            //lbl_title_btm.text = "By continuing, ou agree to Stanbic Bank Terms\n and Counditions and our Privacy policy."
     
        View_box.layer.borderWidth = 1
        View_box.layer.borderColor = UIColor.white.cgColor
        
        btnCountry.layer.borderWidth = 1
        btnCountry.layer.borderColor = UIColor.white.cgColor
        
        let stringValue = "By continuing, ou agree to Stanbic Bank Terms\n and Counditions and our Privacy policy."
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: stringValue)
        attributedString.setColorForText(textForAttribute: "Terms\n and Counditions")
        attributedString.setColorForText(textForAttribute: "Privacy policy.")
        lbl_title_btm.attributedText = attributedString
        txt_mob.text = "700707453"
    }
    
    
       
    
    // MARK:_ keyboardWillShow
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            print(keyboardHeight)
            
            
            btm_button_constant.constant = keyboardHeight - 10
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            
        }
    }
        // MARK:_ keyboardWillHide
        @objc func keyboardWillHide(_ notification: Notification) {
            btm_button_constant.constant = 30
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
     }
    
    
    // Mark:- Btn Continue-
    @IBAction func btn_Continue(_ sender: UIButton) {
        
        txt_mob.resignFirstResponder()
        //CHECK VALID MOBILE NO
        let isValidMob = checkValidMobileNo()
        if isValidMob == false {
            
//            imgLineTxtField.backgroundColor = colorConstant.kRedColor
            return
        }
        
        //Call API for aunthication
        if isInternetAvailable() == false {
            
            self.showMessage(title: "Error", message: "No internet connection!")
            return
            
        }
        else {
            
            let activtyIndicator = nvIndicator()
           activtyIndicator.showActivityIndicator(view: self.view)
            //Call API for send activtion code to mobile no.
            RestAPIManager.sharedInstance.get_validation_key(mobileno: ("254\(txt_mob.text!)")) { (json) in
                
                print(json)
                DispatchQueue.main.async {
                    
                    //Hide Indicator
                    activtyIndicator.removeActivityIndicator()
                    
                    if json["STATUS_CODE"].intValue == 200 {
                        
                        print(json)
                        
                        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
                        nextvc.mob = self.txt_mob.text!
                        nextvc.otp =  json["ACTIVATION_KEY"].stringValue
                         self.present(nextvc, animated: true, completion: nil)
                    
                   
                        //TODO
//                        if AppMode.appMode != ApplicationMode.production {
//                            otpVeri.otp = json["ACTIVATION_KEY"].stringValue
//                        }
//
//                        self.navigationController?.pushViewController(otpVeri, animated: true)
                    }
                    else {
                        self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                        
                    }
                }
            }
        }
    }
    
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
     // MARK:_Check valid mobile no.
    func checkValidMobileNo() -> Bool {
        
        if self.txt_mob.text?.count == 10 {
            
            let prefix = String((self.txt_mob.text?.prefix(2))!)
            if prefix != "07" {
                self.txt_mob.showError(errStr: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
//                imgLineTxtField.backgroundColor = .red
                //showAlert(message: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
                return false
            }
            return true
            
        }
        else if self.txt_mob.text?.count == 9 {
            let prefix = String((self.txt_mob.text?.prefix(1))!)
            if prefix == "0" {
                
                return false
                
            }
            else if prefix != "7" {
                self.txt_mob.showError(errStr: "Invalid mobile number. Enter 9 digit mobile number in the format 7XXX.")
//                imgLineTxtField.backgroundColor = .red
                //showAlert(message: "Invalid mobile number. Enter 9 digit mobile number in the format 7XXX.")
                return false
            }
            
            return true
        }
        else {
            // self.txtMobNo.showError(errStr: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
            // imgLineTxtField.backgroundColor = .red
            //showAlert(message: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
            return false
        }
        
    }
    
   
   
}

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String) {
      let color =   UIColor(red: 0, green: 123, blue: 255, alpha: 1.0)  
        
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        
        // Swift 4.1 and below
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
}
