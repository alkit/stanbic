//
//  ReviewDetailViewController.swift
//  Stanbic_bank
//
//  Created by Vijay Patidar on 14/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class ReviewDetailViewController: UIViewController {

    @IBOutlet weak var lblEmail: UITextField!
    @IBOutlet weak var lblOccupationDetail: UITextField!
    @IBOutlet weak var lblJobIndustry: UITextField!
    @IBOutlet weak var lblSalary: UITextField!
    @IBOutlet weak var lblAddress: UITextField!
    @IBOutlet weak var lblBranch: UITextField!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet weak var relationStatus: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    @IBOutlet weak var kinIdNumber: UITextField!
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    let activtyIndicator = nvIndicator()
    var strEmail = ""
    var strOccupation = ""
    var strJobIndustry = ""
    var strSalary = ""
    var strAddress = ""
    var strPrefferedBranch = ""
    var strFullName = ""
    var strRelation = ""
    var strMobile = ""
    var strKinID = ""
    
    var occupationID = ""
    var jobIndustryID = ""
    var prefferedBranchID = ""
    var relationID = ""
    
    var occupationDict = [String:String]()
    var nextOfKinDict = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblEmail.text = emailValue
        self.lblOccupationDetail.text = strOccupation
        self.lblJobIndustry.text = strJobIndustry
        self.lblSalary.text = strSalary
        self.lblAddress.text = strAddress
        self.lblBranch.text = strPrefferedBranch
        self.fullName.text = strFullName
        self.relationStatus.text = strRelation
        self.mobileNumber.text = strMobile
        self.kinIdNumber.text = strKinID
        
        occupationDict = ["EMPLOYMENT_STATUS": occupationID, "INDUSTRY": jobIndustryID,
            "INCOME": strSalary]
        
    }
    
    @IBAction func btnActionBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnActionConfirm(_ sender: UIButton)
    {
        self.callApi()
    }
    
    func callApi(){
        
        if isInternetAvailable() == false {
            
            self.showMessage(title: "Error", message: "No internet connection!")
            return
            
        }
        else {
            
            //      let activtyIndicator = nvIndicator()
            //      activtyIndicator.showActivityIndicator(view: self.view)
            activtyIndicator.showActivityIndicator(view: self.view)
            //Call API for send activtion code to mobile no.
            RestAPIManager.sharedInstance.IDValidationFormData(OCCUPATION: occupationDict, NEXT_OF_KIN: nextOfKinDict, RESIDENCE: strAddress, EMAIL: strEmail, BRANCH: strPrefferedBranch, onCompletion:
            { (json) in
                
                print(json)
               // strMobileNumber = self.mobno
                DispatchQueue.main.async {
                    
                    //Hide Indicator
                    self.activtyIndicator.removeActivityIndicator()
                    
                    if json["STATUS_CODE"].intValue == 200 {
                                    
                        let nextvc = DetailsValidated.getVCInstance()
                       
                        self.present(nextvc, animated: true, completion: nil)
                    }
                    else {
                        self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                        
                        //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
                    }
                }
            })
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
