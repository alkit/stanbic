
//
//  TakefrontsidePhoto_VC.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 13/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.

import UIKit
import Photos

class TakefrontsidePhoto_VC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    @IBOutlet weak var lbl_stepTitle1: UILabel!
    @IBOutlet weak var lbl_stepTitle2: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_frontside: UIImageView!

    @IBOutlet weak var btn_countinuepic: UIButton!
    @IBOutlet weak var btn_takephoto: UIButton!
    
    var mobno = ""
    var idnumber = ""
    var imgfront = ""
    
     var imagePicker = UIImagePickerController()
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_countinuepic.isHidden = true
        
           //   self.img_frontside.image = UIImage.init(named: "flag.png")
        
        lbl_stepTitle2.layer.masksToBounds = true
          lbl_stepTitle2.layer.cornerRadius = 14
        lbl_stepTitle2.layer.borderWidth = 1
    lbl_stepTitle2.layer.borderColor =  UIColor.black.cgColor
//lbl_stepTitle2.layer.borderColor = UIColor.init(red: 134, green: 142, blue: 150, alpha: 1).cgColor
        
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        
     }
    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(true)
        
    }
    
     // Mark:- BACK-
    @IBAction func btn_Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Mark:- btn_Takephoto-
    @IBAction func btn_Countinuethisphoto(_ sender: UIButton) {
        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TakebacksidePhoto_VC") as! TakebacksidePhoto_VC
        
        nextvc.mobno = mobno
        nextvc.imgfront = imgfront
        nextvc.idnumber = idnumber
        
        nextvc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
        nextvc.BRANCHES_ARRAY =  BRANCHES_ARRAY
        nextvc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
        nextvc.IncomeArray =  IncomeArray
        nextvc.INDUSTRY_ARRAY = self.INDUSTRY_ARRAY
        
        self.present(nextvc, animated: true, completion: nil)
        
    }

    // Mark:- btn_Takephoto-
    @IBAction func btn_TakePhoto(_ sender: UIButton) {
       
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            //self.myPickerController = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        //imagePicker.sourceType = .camera
      //  present(imagePicker, animated: true, completion: nil)
    }

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // Post a notification
            let imageData = image.jpegData(compressionQuality: 0.2)
            self.img_frontside.image = UIImage(data: imageData!)
            imgfront = convertImageToBase64(image: img_frontside.image!)
            lbl_Title.isHidden = true
            
            btn_countinuepic.isHidden = false
            if imageData != nil
            {
                let imageSize: Int = imageData?.count ?? 0
                print("size of image in KB: %f ", Double(imageSize) / 1024.0)
            }
          
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}
