

//
//  ReadySignature_VC.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 14/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class ReadySignature_VC: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    
    var imagePicker = UIImagePickerController()
    var selfistring = ""
    var email = ""
    var id = ""
  //  var signatureDataString = ""
    var data = false
    let img = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
override func viewWillAppear(_ animated: Bool) {
     super.viewWillAppear(true)
    
        if data ==  true {
    let nextvc = TakeSignature_VC.getVCInstance() as! TakeSignature_VC
    nextvc.selfistring = selfistring
    nextvc.email = email
    nextvc.id = id
    nextvc.imgsignature.image = img.image
            data = false
            nextvc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
            nextvc.BRANCHES_ARRAY =  BRANCHES_ARRAY
            nextvc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
            nextvc.IncomeArray =  IncomeArray
            nextvc.INDUSTRY_ARRAY = INDUSTRY_ARRAY
            
    self.present(nextvc, animated: true, completion: nil)
       }
    
    }
    
    // Mark:- btn_Ready FOR Selfie-
    @IBAction func btn_Ready(_ sender: UIButton) {
        
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            //self.myPickerController = UIImagePickerController()
            imagePicker.delegate = self;
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
  
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // Post a notification
            let imageData = image.jpegData(compressionQuality: 0.2)
            self.img.image = UIImage(data: imageData!)
            selfistring = convertImageToBase64(image: img.image!)
            
        }
        self.dismiss(animated: true, completion: {
            
            let nextvc = TakeSignature_VC.getVCInstance() as! TakeSignature_VC
            nextvc.selfistring = self.selfistring
            nextvc.email = self.email
            nextvc.id = self.id
            nextvc.imgsignature.image = self.img.image
            self.data = false
            nextvc.NEXT_OF_KIN_ARRAY =  self.NEXT_OF_KIN_ARRAY
            nextvc.BRANCHES_ARRAY =  self.BRANCHES_ARRAY
            nextvc.EMPLOYMEMT_ARRAY =  self.EMPLOYMEMT_ARRAY
            nextvc.IncomeArray =  self.IncomeArray
            self.present(nextvc, animated: true, completion: nil)
        })
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Register", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
}
