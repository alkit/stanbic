//
//  ReviewYourDetailsViewController.swift
//  Stanbic_bank
//
//  Created by Vijay Patidar on 14/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class ReviewYourDetailsViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate{

    @IBOutlet weak var txt_name: UITextField!
    @IBOutlet weak var txt_id_number:UITextField!
    @IBOutlet weak var txt_id_issue_date: UITextField!
    @IBOutlet weak var txt_id_dob:UITextField!
    @IBOutlet weak var txt_nationality: UITextField!
    
    @IBOutlet weak var baseView1: UIView!
    @IBOutlet weak var baseView2: UIView!
    @IBOutlet weak var baseView3: UIView!
    @IBOutlet weak var baseView4: UIView!
    @IBOutlet weak var baseView5: UIView!
    
    @IBOutlet weak var btnOutlet_scanAgain: UIButton!
    @IBOutlet weak var btnOutlet_continue: UIButton!
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    
    var img_frontside: UIImageView = UIImageView()
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK: - Done image capture here
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [AnyHashable: Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
        img_frontside.image = chosenImage as? UIImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btnActionScanAgain(_ sender: UIButton)
    {
        // open camera here to get the image
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func btnActionContinue(_ sender: UIButton)
    {
        // call api here
        if isvalide() == true
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OccupationDetailViewController") as! OccupationDetailViewController
            vc.strFllName = txt_name.text ?? ""
            vc.strIDNUMBER = txt_id_number.text ?? ""
            vc.strIDIssueDate = txt_id_issue_date.text ?? ""
            vc.strDateOfBirth = txt_id_dob.text ?? ""
            vc.strNationality = txt_nationality.text ?? ""
            vc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
            vc.BRANCHES_ARRAY =  BRANCHES_ARRAY
            vc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
            vc.IncomeArray =  IncomeArray
            vc.INDUSTRY_ARRAY = INDUSTRY_ARRAY
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnActionBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func isvalide()-> Bool
    {
        var flag = true
        if txt_name.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter your name")
        }else if txt_id_number.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter ypur ID number ")
        }else if txt_id_issue_date.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter your ID issue date")
        }
        else if txt_id_dob.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter your Date of birth")
        }else if txt_nationality.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter your nationality")
        }
        return flag
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Register", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
}

extension ReviewYourDetailsViewController:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
        
    }
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txt_name
        {
            txt_name.resignFirstResponder()
            txt_id_number.becomeFirstResponder()
        }else  if textField == txt_id_number
        {
            txt_id_number.resignFirstResponder()
            txt_id_issue_date.becomeFirstResponder()
        }else  if textField == txt_id_issue_date
        {
            txt_id_issue_date.resignFirstResponder()
            txt_id_dob.becomeFirstResponder()
        }else  if textField == txt_id_dob
        {
            txt_id_dob.resignFirstResponder()
            txt_nationality.becomeFirstResponder()
        }else if textField == txt_nationality
        {
            txt_nationality.resignFirstResponder()
        }else
        {
        textField.resignFirstResponder()
        }
        return true
    }
    
}
