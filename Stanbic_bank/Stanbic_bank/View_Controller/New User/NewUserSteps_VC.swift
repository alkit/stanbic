



//
//  NewUserSteps_VC.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 13/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class NewUserSteps_VC: UIViewController {

     var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // Mark:- btn_Back-
    @IBAction func btn_Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Mark:- btn_Back-
    @IBAction func btn_Start(_ sender: UIButton) {
        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "EnterId_VC") as! EnterId_VC
        nextvc.mobileNumber = mobileNumber
        self.present(nextvc, animated: true, completion: nil)
    }

}
