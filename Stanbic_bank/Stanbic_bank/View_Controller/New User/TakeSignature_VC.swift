
//
//  TakeSignature_VC.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 14/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class TakeSignature_VC: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    
    @IBOutlet weak var lbl_stepTitle1: UILabel!
    @IBOutlet weak var lbl_stepTitle2: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_frontside: UIImageView!
    
    @IBOutlet weak var btn_countinuepic: UIButton!
    @IBOutlet weak var btn_takephoto: UIButton!
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    
    var imagePicker: UIImagePickerController!
    var imgsignature = UIImageView()
    
  var selfistring = ""
  var signsring = ""
  var email = ""
  var id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        img_frontside.image = imgsignature.image
       // signsring = convertImageToBase64(image:   img_frontside.image!)
    }
    
    // Mark:- BACK-
    @IBAction func btn_Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Mark:- btn_Takephoto-
    @IBAction func btn_Countinuethisphoto(_ sender: UIButton) {
        self.UploadSignature()
//        let nextVc = ReviewYourDetailsViewController.getVCInstance()
//        self.present(nextVc, animated: true, completion: nil)
//
    }
    
    // Mark:- btn_Takephoto-
    @IBAction func btn_TakePhoto(_ sender: UIButton) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    //MARK: - Done image capture here
     private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [AnyHashable: Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
        signsring = convertImageToBase64(image: chosenImage as! UIImage)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    // UPLOAD SIGNATURE AND Selfi
    func UploadSignature() {
        
        if isInternetAvailable() == false {
            
            self.showMessage(title: "Error", message: "No internet connection!")
            return
            
        }
        else {
            
            let tempImage = UIImage.init(named: "flag.png")
            selfistring = convertImageToBase64(image: tempImage!)
            signsring = convertImageToBase64(image: tempImage!)
                 let activtyIndicator = nvIndicator()
                activtyIndicator.showActivityIndicator(view: self.view)
            
            //Call API for send activtion code to mobile no.
            RestAPIManager.sharedInstance.IDValidationSignature(email: email, kraPIN:id , selfie: selfistring, signature: signsring, onCompletion:
            { (json) in
                
                print(json)
                DispatchQueue.main.async {
                    
                    //Hide Indicator
                      activtyIndicator.removeActivityIndicator()
                    
                    if json["STATUS_CODE"].intValue == 200 {
                        
                        let nextVc = OccupationDetailViewController.getVCInstance() as! OccupationDetailViewController
                        nextVc.NEXT_OF_KIN_ARRAY =  self.NEXT_OF_KIN_ARRAY
                        nextVc.BRANCHES_ARRAY =  self.BRANCHES_ARRAY
                        nextVc.EMPLOYMEMT_ARRAY =  self.EMPLOYMEMT_ARRAY
                        nextVc.IncomeArray =  self.IncomeArray
                        nextVc.INDUSTRY_ARRAY = self.INDUSTRY_ARRAY
                        self.present(nextVc, animated: true, completion: nil)
                        
                    }
                    else {
                        self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                        
                        //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
                    }
                }
            })
        }
        
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Register", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
}


