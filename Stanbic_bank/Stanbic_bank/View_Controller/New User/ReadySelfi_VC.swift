



//
//  ReadySelfi_VC.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 13/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class ReadySelfi_VC: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    var imagePicker: UIImagePickerController!
    var data =  false
    let img = UIImageView()
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
     }
    
    override func viewWillAppear(_ animated: Bool) {
      
        super.viewWillAppear(true)
        
        if data ==  true {
            let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TakeSelfie_VC") as! TakeSelfie_VC
            nextvc.imgselfi.image = img.image
            data = false
            nextvc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
            nextvc.BRANCHES_ARRAY =  BRANCHES_ARRAY
            nextvc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
            nextvc.IncomeArray =  IncomeArray
            nextvc.INDUSTRY_ARRAY = self.INDUSTRY_ARRAY
            self.present(nextvc, animated: true, completion: nil)
        }
    }
    
    // Mark:- btn_Ready FOR Selfie-
    @IBAction func btn_Ready(_ sender: UIButton) {
      imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    //MARK: - Done image capture here
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [AnyHashable: Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
        data = true
        img.image = (chosenImage as! UIImage)
       
        
         picker.dismiss(animated: true, completion: {
            let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TakeSelfie_VC") as! TakeSelfie_VC
            nextvc.imgselfi.image = self.img.image
            nextvc.NEXT_OF_KIN_ARRAY =  self.NEXT_OF_KIN_ARRAY
            nextvc.BRANCHES_ARRAY =  self.BRANCHES_ARRAY
            nextvc.EMPLOYMEMT_ARRAY =  self.EMPLOYMEMT_ARRAY
            nextvc.IncomeArray =  self.IncomeArray
            nextvc.INDUSTRY_ARRAY = self.INDUSTRY_ARRAY
            self.present(nextvc, animated: true, completion: nil)
         })
        
          }
    
   
    
 }
