//
//  OccupationDetailViewController.swift
//  Stanbic_bank
//
//  Created by Vijay Patidar on 14/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class OccupationDetailViewController: UIViewController {

    @IBOutlet weak var txt_occupation: UITextField!
    @IBOutlet weak var txt_job:UITextField!
    @IBOutlet weak var txt_salary: UITextField!
    @IBOutlet weak var txt_branch:UITextField!
    @IBOutlet weak var txt_address:UITextField!
    
    @IBOutlet weak var baseView1: UIView!
    @IBOutlet weak var baseView2: UIView!
    @IBOutlet weak var baseView3: UIView!
    @IBOutlet weak var baseView4: UIView!
    @IBOutlet weak var baseView5: UIView!
    
    var strFllName = ""
    var strIDNUMBER = ""
    var strIDIssueDate = ""
    var strDateOfBirth = ""
    var strNationality = ""
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    
    var indexForPopUp = 0
    var occupationID = ""
    var jobIndustryID = ""
    var prefferedBranchID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnActionBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnActionOccupation(_ sender: UIButton)
    {
       indexForPopUp = 1
        print(self.EMPLOYMEMT_ARRAY)
        self.showPopUp(sender: sender, arrayString: self.EMPLOYMEMT_ARRAY)
    }
    
    @IBAction func btnActionJobIndustry(_ sender: UIButton)
    {
        indexForPopUp = 2
         self.showPopUp(sender: sender, arrayString: INDUSTRY_ARRAY_DATA)
    }
    @IBAction func btnActionPrefferedBranch(_ sender: UIButton)
    {
        indexForPopUp = 3
         self.showPopUp(sender: sender, arrayString: self.BRANCHES_ARRAY)
    }
    
    @IBAction func btnActionContinue(_ sender: UIButton)
    {
        if isvalide() == true
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "KinInformationViewController") as! KinInformationViewController
            vc.strFllName = strFllName
            vc.strIDNUMBER = strIDNUMBER
            vc.strIDIssueDate = strIDIssueDate
            vc.strDateOfBirth = strDateOfBirth
            vc.strNationality = strNationality
            
            vc.strOccupationStatus = txt_occupation.text ?? ""
            vc.strJobIndustry = txt_job.text ?? ""
            vc.strGrossSalary = txt_salary.text ?? ""
            vc.strLive = txt_address.text ?? ""
            vc.strPrefferedBranch = txt_branch.text ?? ""
            vc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
            vc.BRANCHES_ARRAY =  BRANCHES_ARRAY
            vc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
            vc.IncomeArray =  IncomeArray
            vc.INDUSTRY_ARRAY = INDUSTRY_ARRAY
            
            vc.occupationID = occupationID
            vc.jobIndustryID = jobIndustryID
            vc.prefferedBranchID = prefferedBranchID
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    static func getVCInstance() -> UIViewController{
        // This method returns the instance on it self to push or present in Navigation.
        return UIStoryboard(name: "Register", bundle: .main).instantiateViewController(withIdentifier: "\(String(describing: self))")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func isvalide()-> Bool
    {
        var flag = true
        if txt_occupation.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please select occupation status")
        }else if txt_job.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please select your job industry ")
        }else if txt_salary.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter your average gross salary")
        }
        else if txt_address.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter your address")
        }else if txt_branch.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please select your preffered branch")
        }
        return flag
    }
    
    func showPopUp(sender:UIButton , arrayString : [String])
    {
        let popupVC = PopupVC.getVCInstance() as! PopupVC
        print(arrayString)
        popupVC.valueArr = arrayString // string array
        popupVC.modalPresentationStyle = UIModalPresentationStyle.popover
        popupVC.preferredContentSize = CGSize(width: 200, height: 87)
        popupVC.popoverPresentationController?.delegate = self
        popupVC.delegate = self
        self.present(popupVC, animated: true)
        
        
        let popover = popupVC.popoverPresentationController
        popover!.sourceView = sender
        popover!.sourceRect = sender.bounds
        
    }

}


extension OccupationDetailViewController: PopupVCDelegate{
    func getIndex(indx: Int) {
        // todo index
//        var occupationID = ""
//        var jobIndustryID = ""
//        var prefferedBranchID = ""
      if indexForPopUp == 1
      {
        occupationID = EMPLOYMEMT_ARRAY_ID[indx]
        txt_occupation.text = EMPLOYMEMT_ARRAY[indx]
      }else if indexForPopUp == 2
      {
        jobIndustryID = INDUSTRY_ARRAY_ID[indx]
        txt_job.text = INDUSTRY_ARRAY_DATA[indx]
      }else if indexForPopUp == 3
      {
        prefferedBranchID = BRANCHES_ARRAY_ID[indx]
        txt_branch.text = self.BRANCHES_ARRAY[indx]
      }
        
    }
    
    
}
extension OccupationDetailViewController: UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return UIModalPresentationStyle.none
    }
}

extension OccupationDetailViewController:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
        
    }
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txt_salary
        {
            txt_salary.resignFirstResponder()
            txt_address.becomeFirstResponder()
        }else  if textField == txt_address
        {
            txt_address.resignFirstResponder()
           
        }else
        {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
