
//
//  FillPersonalDetail_VC.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 13/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit
var emailValue = ""

class FillPersonalDetail_VC: UIViewController {
    
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_krapinr:UITextField!
    @IBOutlet weak var baseView1: UIView!
    @IBOutlet weak var baseView2: UIView!
    
    var selfistring = ""
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        baseView1.layer.borderWidth = 1
        baseView1.layer.cornerRadius = 4
        baseView2.layer.borderWidth = 1
        baseView2.layer.cornerRadius = 4
        
        txt_email.addUI(placeholder: "Enter Email")
        
        txt_krapinr.addUI(placeholder: "Enter KRA PIN")
    }
    
    
    
    
    
    // Mark:- btn_Back-
    @IBAction func btn_Continue(_ sender: UIButton) {
        if txt_email.text == "" {
            self.showMessage(title: "Error", message: "Please Enter Email Address")
        }
        else if txt_krapinr.text == "" {
            self.showMessage(title: "Error", message: "Please Enter KRA Pin")
        }
        else {
            // ReadySignature_VC
            let nextvc = ReadySignature_VC.getVCInstance() as! ReadySignature_VC
            nextvc.selfistring = selfistring
            nextvc.email = txt_email.text!
            nextvc.id = txt_krapinr.text!
            nextvc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
            nextvc.BRANCHES_ARRAY =  BRANCHES_ARRAY
            nextvc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
            nextvc.IncomeArray =  IncomeArray
            nextvc.INDUSTRY_ARRAY = INDUSTRY_ARRAY
            emailValue = txt_email.text ?? ""
            self.present(nextvc, animated: true, completion: nil)
            
        }
    }
}

extension FillPersonalDetail_VC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txt_email
        {
            txt_email.resignFirstResponder()
            txt_krapinr.becomeFirstResponder()
        }else if textField == txt_krapinr
        {
            txt_krapinr.resignFirstResponder()
        }else
        {
            textField.resignFirstResponder()
        }
        return false
    }
}
