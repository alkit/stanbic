




//
//  EnterId_VC.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 13/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

var NEXT_OF_KIN_ARRAY_ID:[String] = [String]()
var BRANCHES_ARRAY_ID:[String] = [String]()
var EMPLOYMEMT_ARRAY_ID:[String] = [String]()
var IncomeArray_ID:[String] = [String]()
var INDUSTRY_ARRAY_ID:[String] = [String]()
var INDUSTRY_ARRAY_DATA:[String] = [String]()

class EnterId_VC: UIViewController {
    
       @IBOutlet weak var txt_mob: UITextField!
    
    var dict:Result!
     var mobileNumber = ""
    var INCOMEOne:String!
    var INCOMEtwo:String!
    var INCOMEThree:String!
    var INCOMEFour:String!

    
    var NEXT_OF_KIN_One:String!
    var NEXT_OF_KIN_Two:String!
    var NEXT_OF_KIN_Three:String!
    var NEXT_OF_KIN_Four:String!
    var NEXT_OF_KIN_Five:String!
    var NEXT_OF_KIN_Six:String!
    
  
    
    var BRANCHES_027:String!
    var BRANCHES_025:String!
    var BRANCHES_019:String!
    var BRANCHES_010:String!
    var BRANCHES_018:String!
    var BRANCHES_016:String!
    var BRANCHES_001:String!
    var BRANCHES_150:String!
    var BRANCHES_393:String!
    var BRANCHES_057:String!
    var BRANCHES_059:String!
    
    
    
    var INDUSTRY_One:String!
    var INDUSTRY_Two:String!
    var INDUSTRY_Three:String!
    
    var INDUSTRY_ARRAY:[String] = [String]()
    
    var EMPLOYMENT_STATUS_One:String!
    var EMPLOYMENT_STATUS_Two:String!
    var EMPLOYMENT_STATUS_Three:String!
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var txt_idnumber: UITextField!
    var arrayb = [ProductSuggestion]()
    
    struct QueData{
        var key : String
        var data : [String: JSON]
    }
    
        var queArr = [QueData]()
    
    var mobno = ""
    var idnumber = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        baseView.layer.cornerRadius = 4
        baseView.layer.borderWidth = 1
        baseView.layer.borderColor = UIColor.lightGray.cgColor
       txt_idnumber.addUI(placeholder:"ID NUMBER")
        Fetch()
        
      txt_mob.text = mobileNumber
      txt_mob.isUserInteractionEnabled = false
        //--- add UIToolBar on keyboard and Done button on UIToolBar ---//
        self.addDoneButtonOnKeyboard()
        addDoneButtonOnKeyboard1()
    }
    
    //--- *** ---//
    
    func addDoneButtonOnKeyboard1(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonMob))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txt_mob.inputAccessoryView = doneToolbar
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        txt_idnumber.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        txt_idnumber.resignFirstResponder()
    }
    
    @objc func doneButtonMob(){
        txt_mob.resignFirstResponder()
    }
    
    // Mark:- BACK-
    @IBAction func btn_Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
  
    func Fetch(){
        
       //Call API for aunthication
        if isInternetAvailable() == false {
            
            self.showMessage(title: "Error", message: "No internet connection!")
            return
            
        }
        else {
            
                       let activtyIndicator = nvIndicator()
                        activtyIndicator.showActivityIndicator(view: self.view)
            //Call API for send activtion code to mobile no.
            RestAPIManager.sharedInstance.FetchesAccountopeningdata(mobileno:"700707453")
            { (json) in
                
                DispatchQueue.main.async {
                    
                    //Hide Indicator
                      activtyIndicator.removeActivityIndicator()
                    
                    if json["STATUS_CODE"].intValue == 200 {
                        
                        print(json)
                        
                        self.INCOMEOne = json["RESULT_ARRAY"]["INCOME"]["1"].stringValue
                        self.INCOMEtwo = json["RESULT_ARRAY"]["INCOME"]["2"].stringValue
                        self.INCOMEThree = json["RESULT_ARRAY"]["INCOME"]["3"].stringValue
                        self.INCOMEFour = json["RESULT_ARRAY"]["INCOME"]["4"].stringValue
                        
                        IncomeArray_ID.append("1")
                        IncomeArray_ID.append("2")
                        IncomeArray_ID.append("3")
                        IncomeArray_ID.append("4")
                        
                        self.IncomeArray.append(self.INCOMEOne)
                        self.IncomeArray.append(self.INCOMEtwo)
                        self.IncomeArray.append(self.INCOMEThree)
                        self.IncomeArray.append(self.INCOMEFour)
                        
                        self.NEXT_OF_KIN_One = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["1"].stringValue
                        self.NEXT_OF_KIN_Two = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["2"].stringValue
                        self.NEXT_OF_KIN_Three = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["3"].stringValue
                        self.NEXT_OF_KIN_Four = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["4"].stringValue
                        self.NEXT_OF_KIN_Five = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["5"].stringValue
                        self.NEXT_OF_KIN_Six = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["6"].stringValue
                        
                        self.NEXT_OF_KIN_ARRAY.append(self.NEXT_OF_KIN_One)
                        self.NEXT_OF_KIN_ARRAY.append(self.NEXT_OF_KIN_Two)
                        self.NEXT_OF_KIN_ARRAY.append(self.NEXT_OF_KIN_Three)
                        self.NEXT_OF_KIN_ARRAY.append(self.NEXT_OF_KIN_Four)
                        self.NEXT_OF_KIN_ARRAY.append(self.NEXT_OF_KIN_Five)
                        self.NEXT_OF_KIN_ARRAY.append(self.NEXT_OF_KIN_Six)
                        
                        NEXT_OF_KIN_ARRAY_ID.append("1")
                        NEXT_OF_KIN_ARRAY_ID.append("2")
                        NEXT_OF_KIN_ARRAY_ID.append("3")
                        NEXT_OF_KIN_ARRAY_ID.append("4")
                        NEXT_OF_KIN_ARRAY_ID.append("5")
                        NEXT_OF_KIN_ARRAY_ID.append("6")
                        
                        self.INDUSTRY_One = json["RESULT_ARRAY"]["INDUSTRY"]["1"].stringValue
                        self.INDUSTRY_Two = json["RESULT_ARRAY"]["INDUSTRY"]["2"].stringValue
                        self.INDUSTRY_Three = json["RESULT_ARRAY"]["INDUSTRY"]["3"].stringValue
                        
                        self.INDUSTRY_ARRAY.append(self.INDUSTRY_One)
                        self.INDUSTRY_ARRAY.append(self.INDUSTRY_Two)
                        self.INDUSTRY_ARRAY.append(self.INDUSTRY_Three)
                        
                        INDUSTRY_ARRAY_ID.append("1")
                        INDUSTRY_ARRAY_ID.append("2")
                        INDUSTRY_ARRAY_ID.append("3")
                        
                        INDUSTRY_ARRAY_DATA.append(self.INDUSTRY_One)
                        INDUSTRY_ARRAY_DATA.append(self.INDUSTRY_Two)
                        INDUSTRY_ARRAY_DATA.append(self.INDUSTRY_Three)
                        //INDUSTRY_ARRAY_DATA
                        
                        self.EMPLOYMENT_STATUS_One = json["RESULT_ARRAY"]["EMPLOYMENT_STATUS"]["1"].stringValue
                        self.EMPLOYMENT_STATUS_Two = json["RESULT_ARRAY"]["EMPLOYMENT_STATUS"]["2"].stringValue
                        self.EMPLOYMENT_STATUS_Three = json["RESULT_ARRAY"]["EMPLOYMENT_STATUS"]["3"].stringValue
                        
                       
                        self.EMPLOYMEMT_ARRAY.append(self.EMPLOYMENT_STATUS_One)
                        self.EMPLOYMEMT_ARRAY.append(self.EMPLOYMENT_STATUS_Two)
                        self.EMPLOYMEMT_ARRAY.append(self.EMPLOYMENT_STATUS_Three)
                        
                        EMPLOYMEMT_ARRAY_ID.append("1")
                        EMPLOYMEMT_ARRAY_ID.append("2")
                        EMPLOYMEMT_ARRAY_ID.append("3")
                        
                        self.BRANCHES_027 = json["RESULT_ARRAY"]["BRANCHES"]["027"].stringValue
                        self.BRANCHES_025 = json["RESULT_ARRAY"]["BRANCHES"]["025"].stringValue
                        self.BRANCHES_019 = json["RESULT_ARRAY"]["BRANCHES"]["019"].stringValue
                        self.BRANCHES_010 = json["RESULT_ARRAY"]["BRANCHES"]["010"].stringValue
                        self.BRANCHES_018 = json["RESULT_ARRAY"]["BRANCHES"]["018"].stringValue
                        self.BRANCHES_016 = json["RESULT_ARRAY"]["BRANCHES"]["016"].stringValue
                        self.BRANCHES_001 = json["RESULT_ARRAY"]["BRANCHES"]["001"].stringValue
                        self.BRANCHES_150 = json["RESULT_ARRAY"]["BRANCHES"]["150"].stringValue
                        self.BRANCHES_393 = json["RESULT_ARRAY"]["BRANCHES"]["393"].stringValue
                        self.BRANCHES_057 = json["RESULT_ARRAY"]["BRANCHES"]["057"].stringValue
                        self.BRANCHES_059 = json["RESULT_ARRAY"]["BRANCHES"]["059"].stringValue
                        
                        self.BRANCHES_ARRAY.append(self.BRANCHES_027)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_025)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_019)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_010)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_018)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_016)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_001)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_150)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_393)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_057)
                        self.BRANCHES_ARRAY.append(self.BRANCHES_059)
                        
                        BRANCHES_ARRAY_ID.append("027")
                        BRANCHES_ARRAY_ID.append("025")
                        BRANCHES_ARRAY_ID.append("019")
                        BRANCHES_ARRAY_ID.append("010")
                        BRANCHES_ARRAY_ID.append("018")
                        BRANCHES_ARRAY_ID.append("016")
                        BRANCHES_ARRAY_ID.append("001")
                        BRANCHES_ARRAY_ID.append("150")
                        BRANCHES_ARRAY_ID.append("393")
                        BRANCHES_ARRAY_ID.append("057")
                        BRANCHES_ARRAY_ID.append("059")
                        
                    }
                    else {
                        self.showMessage(title: "error", message:  json["STATUS_MESSAGE"].stringValue)
                        
                        //self.showAlert(message: json["STATUS_MESSAGE"].stringValue)
                    }
                }
            }
        }
    }
    
    
    // Mark:- btn_Back-
    @IBAction func btn_Continue(_ sender: UIButton) {
       
        let isValidMob = checkValidMobileNo()
//        if isValidMob == false {
//            self.showMessage(title: "Error", message:"Enter Valid Mobile Number")
//        }
//        else
            if txt_idnumber.text == "" {
            self.showMessage(title: "Error", message:"Enter ID Number")
        }
        else{
        
        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "TakefrontsidePhoto_VC") as! TakefrontsidePhoto_VC
            nextvc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
            nextvc.BRANCHES_ARRAY =  BRANCHES_ARRAY
            nextvc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
            nextvc.IncomeArray =  IncomeArray
            nextvc.INDUSTRY_ARRAY = self.INDUSTRY_ARRAY
            nextvc.mobno = txt_mob.text!
                        nextvc.idnumber = txt_idnumber.text!
        self.present(nextvc, animated: true, completion: nil)
        }
    }
    
    
    // MARK:_Check valid mobile no.
    func checkValidMobileNo() -> Bool {
        
        if self.txt_mob.text?.count == 10 {
            
            let prefix = String((self.txt_mob.text?.prefix(2))!)
            if prefix != "07" {
                self.txt_mob.showError(errStr: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
                // imgLineTxtField.backgroundColor = .red
                //showAlert(message: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
                return false
            }
            return true
            
        }
        else if self.txt_mob.text?.count == 9 {
            let prefix = String((self.txt_mob.text?.prefix(1))!)
            if prefix == "0" {
                
                return false
                
            }
            else if prefix != "7" {
                self.txt_mob.showError(errStr: "Invalid mobile number. Enter 9 digit mobile number in the format 7XXX.")
                //  imgLineTxtField.backgroundColor = .red
                //showAlert(message: "Invalid mobile number. Enter 9 digit mobile number in the format 7XXX.")
                return false
            }
            
            return true
        }
        else {
            // self.txtMobNo.showError(errStr: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
            // imgLineTxtField.backgroundColor = .red
            //showAlert(message: "Invalid mobile number. Enter 10 digit mobile number in the format 07XXX.")
            return false
        }
        
    }
    

}

extension EnterId_VC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
        
    }
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txt_idnumber
        {
            txt_idnumber.resignFirstResponder()
            
        }else
        {
            textField.resignFirstResponder()
        }
        return true
    }
}
