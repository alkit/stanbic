//
//  KinInformationViewController.swift
//  Stanbic_bank
//
//  Created by Vijay Patidar on 14/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class KinInformationViewController: UIViewController {

    
    @IBOutlet weak var txt_name:UITextField!
    @IBOutlet weak var txt_relation: UITextField!
    @IBOutlet weak var txt_id:UITextField!
    @IBOutlet weak var txt_mobile_no: UITextField!
    
    @IBOutlet weak var baseView1: UIView!
    @IBOutlet weak var baseView2: UIView!
    @IBOutlet weak var baseView3: UIView!
    @IBOutlet weak var baseView4: UIView!
    
    var strFllName = ""
    var strIDNUMBER = ""
    var strIDIssueDate = ""
    var strDateOfBirth = ""
    var strNationality = ""
    
    var strOccupationStatus = ""
    var strJobIndustry = ""
    var strGrossSalary = ""
    var strLive = ""
    var strPrefferedBranch = ""
    
    var occupationID = ""
    var jobIndustryID = ""
    var prefferedBranchID = ""
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    var relationID = ""
    
    var nextOfKinDict = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnActionBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnACTIONDROPDOWNRELATION(_ sender: UIButton)
    {
        self.showPopUp(sender: sender, arrayString: self.NEXT_OF_KIN_ARRAY)
    }
    
    @IBAction func btnActionContinue(_ sender: UIButton)
    {
        if isvalide() == true
        {
            // send the data to another controller
            
            nextOfKinDict = ["NAME": txt_name.text ?? "", "RELATIONSHIP": relationID, "ID_NUMBER": txt_id.text ?? "", "MSISDN": txt_mobile_no.text ?? ""]
            
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewDetailViewController") as! ReviewDetailViewController
            
            vc.nextOfKinDict = nextOfKinDict
            vc.strFullName = txt_name.text ?? ""
            vc.strKinID = txt_id.text ?? ""
            vc.strOccupation = strOccupationStatus
            vc.strJobIndustry = strJobIndustry
            vc.strSalary = strGrossSalary
            
            vc.strAddress = strLive
            vc.strPrefferedBranch = strPrefferedBranch
            vc.strRelation = txt_relation.text ?? ""
            vc.strMobile = txt_mobile_no.text ?? ""
            vc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
            vc.BRANCHES_ARRAY =  BRANCHES_ARRAY
            vc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
            vc.IncomeArray =  IncomeArray
            vc.INDUSTRY_ARRAY = INDUSTRY_ARRAY
        
            vc.occupationID = occupationID
            vc.jobIndustryID = jobIndustryID
            vc.prefferedBranchID = prefferedBranchID
            vc.relationID = relationID
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    func showPopUp(sender:UIButton , arrayString : [String])
    {
        let popupVC = PopupVC.getVCInstance() as! PopupVC
        print(arrayString)
        popupVC.valueArr = arrayString // string array
        popupVC.modalPresentationStyle = UIModalPresentationStyle.popover
        popupVC.preferredContentSize = CGSize(width: 200, height: 87)
        popupVC.popoverPresentationController?.delegate = self
        popupVC.delegate = self
        self.present(popupVC, animated: true)
        
        
        let popover = popupVC.popoverPresentationController
        popover!.sourceView = sender
        popover!.sourceRect = sender.bounds
        
    }
    
    func isvalide()-> Bool
    {
        var flag = true
        if txt_name.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter full name ")
        }else if txt_relation.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please select your relation ")
        }else if txt_id.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please select your Kin ID ")
        }
        else if txt_mobile_no.text?.isEmpty == true
        {
            flag = false
            self.showMessage(title: "", message: "Please enter your mobile number")
        }
        return flag
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension KinInformationViewController: PopupVCDelegate{
    func getIndex(indx: Int) {
        // todo index
        //        var occupationID = ""
        //        var jobIndustryID = ""
        //        var prefferedBranchID = ""
      
    relationID = NEXT_OF_KIN_ARRAY_ID[indx]
    txt_relation.text = NEXT_OF_KIN_ARRAY[indx]
        
        
    }
    
    
}
extension KinInformationViewController: UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return UIModalPresentationStyle.none
    }
}


extension KinInformationViewController:UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.didBegin()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.didEnd()
        
    }
    // Mark : Text Field Delegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txt_name
        {
            txt_name.resignFirstResponder()
         
        }else  if textField == txt_id
        {
            txt_id.resignFirstResponder()
            txt_mobile_no.becomeFirstResponder()
        }else  if textField == txt_mobile_no
        {
            txt_mobile_no.resignFirstResponder()
            
        }else
        {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
