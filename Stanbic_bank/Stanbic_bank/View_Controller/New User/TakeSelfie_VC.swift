

//
//  TakeSelfie_VC.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 13/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class TakeSelfie_VC: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    
    @IBOutlet weak var lbl_stepTitle1: UILabel!
    @IBOutlet weak var lbl_stepTitle2: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_frontside: UIImageView!
    
    @IBOutlet weak var btn_countinuepic: UIButton!
    @IBOutlet weak var btn_takephoto: UIButton!

    var imagePicker: UIImagePickerController!
    var imgselfi = UIImageView()
    
    var selfistring = ""
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    var BRANCHES_ARRAY:[String] = [String]()
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    var IncomeArray:[String] = [String]()
    var INDUSTRY_ARRAY:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        img_frontside.image = imgselfi.image
       selfistring = convertImageToBase64(image:   img_frontside.image!)
    }
    
    // Mark:- BACK-
    @IBAction func btn_Back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Mark:- btn_Takephoto-
    @IBAction func btn_Countinuethisphoto(_ sender: UIButton) {
        let nextvc = self.storyboard?.instantiateViewController(withIdentifier: "FillPersonalDetail_VC") as! FillPersonalDetail_VC
        nextvc.selfistring = selfistring
        nextvc.NEXT_OF_KIN_ARRAY =  NEXT_OF_KIN_ARRAY
        nextvc.BRANCHES_ARRAY =  BRANCHES_ARRAY
        nextvc.EMPLOYMEMT_ARRAY =  EMPLOYMEMT_ARRAY
        nextvc.IncomeArray =  IncomeArray
        nextvc.INDUSTRY_ARRAY = self.INDUSTRY_ARRAY
        
       self.present(nextvc, animated: true, completion: nil)
        }
    
    
    // Mark:- btn_Takephoto-
    @IBAction func btn_TakePhoto(_ sender: UIButton) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    //MARK: - Done image capture here

    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [AnyHashable: Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage]
        selfistring = convertImageToBase64(image: chosenImage as! UIImage)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        let imageData = image.pngData()!
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
  

}
    


