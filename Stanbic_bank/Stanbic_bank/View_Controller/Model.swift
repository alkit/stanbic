//
//  Model.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 13/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit

class Model: NSObject {

    
    // MARK:_ ROTATE 360 degree-
    func rotate(imageLoader:UIImageView) {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        imageLoader.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    // MARK:_ Stop Rotation-
    func stopRotating(imageLoader:UIImageView) {
        if  imageLoader.layer.animation(forKey:"rotationAnimation") != nil {
            imageLoader.layer.removeAnimation(forKey: "rotationAnimation")
        }
    }
    
}
