//
//  Products.swift
//  ALFAPARF MILANO
//
//  Created by vijay patidar on 19/12/18.
//  Copyright © 2018 5Exceptions. All rights reserved.
//

import Foundation

class ProductSuggestion
{
    var productId:String!
    var productImageUrl:String!
    var productName:String!
    var productType:String!
    var productDescription:String!
    var productFormate = [sub]()
    
    init(with json:JSON)
    {
        self.productId = ""//json["ID"].stringValue
        self.productImageUrl = json["product_image"].stringValue
        self.productName = json["product_name"].stringValue
        self.productType = ""//json["product_type"].stringValue
        self.productDescription = json["product_description"].stringValue
        for value in json["ressj"]["sub"].arrayValue
        {
            productFormate.append(sub(with: value))
        }
    }
}


class sub
{
    var productId:String!
    var productImageUrl:String!
    var productName:String!
    var productType:String!
    var productDescription:String!
    var productFormate:String!
    
    init(with json:JSON)
    {
        self.productId = ""//json["ID"].stringValue
        self.productImageUrl = json["product_image"].stringValue
        self.productName = json["product_name"].stringValue
        self.productType = ""//json["product_type"].stringValue
        self.productDescription = json["product_description"].stringValue
        self.productFormate = json["product_format"].stringValue
    }
}



class TextFieldValidation
{
    var birthlimit:Int!
    var birthQuestion:String!
    var birthType:Int!
    
    var AccLimit:Int!
    var AccQuestion:String!
    var AccType:Int!
    
    var IdLimt:Int!
    var IdQuestion:String!
    var IdType:Int!
    
    init(with json:JSON)
    {
        self.birthlimit = json["2"]["charlimit"].intValue
        self.birthQuestion = json["2"]["question"].stringValue
        self.birthType = json["2"]["dataType"].intValue
        
        self.AccLimit = json["3"]["charlimit"].intValue
        self.AccQuestion = json["3"]["question"].stringValue
        self.AccType = json["3"]["dataType"].intValue
        
        self.IdLimt = json["1"]["charlimit"].intValue
        self.IdQuestion = json["1"]["question"].stringValue
        self.IdType = json["1"]["dataType"].intValue
        
        
        
    }
}



class Result
{
    
    var INCOMEOne:String!
    var INCOMEtwo:String!
    var INCOMEThree:String!
    var INCOMEFour:String!
    var IncomeArray:[String] = [String]()
    
    var NEXT_OF_KIN_One:String!
    var NEXT_OF_KIN_Two:String!
    var NEXT_OF_KIN_Three:String!
    var NEXT_OF_KIN_Four:String!
    var NEXT_OF_KIN_Five:String!
    var NEXT_OF_KIN_Six:String!
    
    var NEXT_OF_KIN_ARRAY:[String] = [String]()
    
    var BRANCHES_027:String!
    var BRANCHES_025:String!
    var BRANCHES_019:String!
    var BRANCHES_010:String!
    var BRANCHES_018:String!
    var BRANCHES_016:String!
    var BRANCHES_001:String!
    var BRANCHES_150:String!
    var BRANCHES_393:String!
    var BRANCHES_057:String!
    var BRANCHES_059:String!
    
    var BRANCHES_ARRAY:[String] = [String]()
    
    var INDUSTRY_One:String!
    var INDUSTRY_Two:String!
    var INDUSTRY_Three:String!
    
    var INDUSTRY_ARRAY:[String] = [String]()
    
    var EMPLOYMENT_STATUS_One:String!
    var EMPLOYMENT_STATUS_Two:String!
    var EMPLOYMENT_STATUS_Three:String!
    
    var EMPLOYMEMT_ARRAY:[String] = [String]()
    
    init(with json:JSON)
    {
        self.INCOMEOne = json["RESULT_ARRAY"]["INCOME"]["1"].stringValue
        self.INCOMEtwo = json["RESULT_ARRAY"]["INCOME"]["2"].stringValue
        self.INCOMEThree = json["RESULT_ARRAY"]["INCOME"]["3"].stringValue
        self.INCOMEFour = json["RESULT_ARRAY"]["INCOME"]["4"].stringValue
        
        self.IncomeArray.append(self.INCOMEOne)
        self.IncomeArray.append(self.INCOMEtwo)
        self.IncomeArray.append(self.INCOMEThree)
        self.IncomeArray.append(self.INCOMEFour)
        
        self.NEXT_OF_KIN_One = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["1"].stringValue
        self.NEXT_OF_KIN_Two = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["2"].stringValue
        self.NEXT_OF_KIN_Three = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["3"].stringValue
        self.NEXT_OF_KIN_Four = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["4"].stringValue
        self.NEXT_OF_KIN_Five = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["5"].stringValue
        self.NEXT_OF_KIN_Six = json["RESULT_ARRAY"]["NEXT_OF_KIN"]["6"].stringValue
        
        self.NEXT_OF_KIN_ARRAY.append(NEXT_OF_KIN_One)
        self.NEXT_OF_KIN_ARRAY.append(NEXT_OF_KIN_Two)
        self.NEXT_OF_KIN_ARRAY.append(NEXT_OF_KIN_Three)
        self.NEXT_OF_KIN_ARRAY.append(NEXT_OF_KIN_Four)
        self.NEXT_OF_KIN_ARRAY.append(NEXT_OF_KIN_Five)
        self.NEXT_OF_KIN_ARRAY.append(NEXT_OF_KIN_Six)
        
        self.INDUSTRY_One = json["RESULT_ARRAY"]["INDUSTRY"]["1"].stringValue
        self.INDUSTRY_Two = json["RESULT_ARRAY"]["INDUSTRY"]["2"].stringValue
        self.INDUSTRY_Three = json["RESULT_ARRAY"]["INDUSTRY"]["3"].stringValue
        
        self.INDUSTRY_ARRAY.append(INDUSTRY_One)
        self.INDUSTRY_ARRAY.append(INDUSTRY_Two)
        self.INDUSTRY_ARRAY.append(INDUSTRY_Three)
        
        self.EMPLOYMENT_STATUS_One = json["RESULT_ARRAY"]["EMPLOYMENT_STATUS"]["1"].stringValue
        self.EMPLOYMENT_STATUS_Two = json["RESULT_ARRAY"]["EMPLOYMENT_STATUS"]["2"].stringValue
        self.EMPLOYMENT_STATUS_Three = json["RESULT_ARRAY"]["EMPLOYMENT_STATUS"]["3"].stringValue
        
        self.EMPLOYMEMT_ARRAY.append(EMPLOYMENT_STATUS_One)
        self.EMPLOYMEMT_ARRAY.append(EMPLOYMENT_STATUS_Two)
        self.EMPLOYMEMT_ARRAY.append(EMPLOYMENT_STATUS_Three)
        
        self.BRANCHES_027 = json["RESULT_ARRAY"]["BRANCHES"]["027"].stringValue
        self.BRANCHES_025 = json["RESULT_ARRAY"]["BRANCHES"]["025"].stringValue
        self.BRANCHES_019 = json["RESULT_ARRAY"]["BRANCHES"]["019"].stringValue
        self.BRANCHES_010 = json["RESULT_ARRAY"]["BRANCHES"]["010"].stringValue
        self.BRANCHES_018 = json["RESULT_ARRAY"]["BRANCHES"]["018"].stringValue
        self.BRANCHES_016 = json["RESULT_ARRAY"]["BRANCHES"]["016"].stringValue
        self.BRANCHES_001 = json["RESULT_ARRAY"]["BRANCHES"]["001"].stringValue
        self.BRANCHES_150 = json["RESULT_ARRAY"]["BRANCHES"]["150"].stringValue
        self.BRANCHES_393 = json["RESULT_ARRAY"]["BRANCHES"]["393"].stringValue
        self.BRANCHES_057 = json["RESULT_ARRAY"]["BRANCHES"]["057"].stringValue
        self.BRANCHES_059 = json["RESULT_ARRAY"]["BRANCHES"]["059"].stringValue
        
        self.BRANCHES_ARRAY.append(BRANCHES_027)
        self.BRANCHES_ARRAY.append(BRANCHES_025)
        self.BRANCHES_ARRAY.append(BRANCHES_019)
        self.BRANCHES_ARRAY.append(BRANCHES_010)
        self.BRANCHES_ARRAY.append(BRANCHES_018)
        self.BRANCHES_ARRAY.append(BRANCHES_016)
        self.BRANCHES_ARRAY.append(BRANCHES_001)
        self.BRANCHES_ARRAY.append(BRANCHES_150)
        self.BRANCHES_ARRAY.append(BRANCHES_393)
        self.BRANCHES_ARRAY.append(BRANCHES_057)
        self.BRANCHES_ARRAY.append(BRANCHES_059)
    }
}
