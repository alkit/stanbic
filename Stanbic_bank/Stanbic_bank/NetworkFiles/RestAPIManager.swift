//
//  RestAPIManager.swift
//  RestAPIJSONProject
//
//  Created by 5Exception-Mac2 on 22/07/16.
//  Copyright © 2016 5Exception-Mac2. All rights reserved.
//

import UIKit

    typealias ServiceResponse = (JSON, NSError?) -> Void
    
    class RestAPIManager: NSObject {
        
        static let sharedInstance = RestAPIManager()
        var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)

        /////////////////////////////////**** BASE URL *****/////////////////////////////////////////////
       // let baseURL = AppMode.appMode?.getBaseUrl() ?? ""
        
        let baseURL = "https://beep2.cellulant.com:9001/MBServiceAPI/"
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        
        /////////////////////////////////**** BASE URL *****/////////////////////////////////////////////
        let clientHash = "$P$B7lwP0IV3fdK4H3oAuttUZiKh3sc7c1"
        
 /*
        func UIDevice() -> String
        {
            var udidArray = CoreDataHelper().getDataForEntity(entity : "DEVICEID")
            if udidArray.count > 0
            {
                let udid = udidArray[0]["aDEVICEID"] ?? ""
                return udid
            }
            else
            {
                CoreDataHelper().saveUUID(value: UIDevice.current.identifierForVendor!.uuidString, entityName: "DEVICEID", key: "DEVICEID", removeRecords: true)
                return UIDevice.current.identifierForVendor!.uuidString
            }
        }
*/
        
        
        //Get Validation Key ! mob
        func get_validation_key(mobileno : String, onCompletion: @escaping (JSON) -> Void) {
            // UIDevice.current.identifierForVendor?.uuidString ?? ""
            let route = baseURL
            let paramDict = [
                "SCOMMAND"      :   sCommand.gAK,
                "UPHONENUM"     :   mobileno,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "parseInstallationID" : "iosvijaygyggy-fffgj-esdgvhh-ji"
                ,"deviceName"   : UIDevice.current.name,
                 "HASH_KEY"     : "IOSDevice"
                ] as [String : Any]
        
            print(paramDict)
            if Session.sharedInstance.isPrint {
                print(paramDict)
                
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                
                onCompletion(json as JSON)
               
            })
            
            
            
            
        }
        
        //Validate Activation Pin ! otp
        func validation_activation_pin(mobileno : String, activation_code : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict = [
                "SCOMMAND"      :   sCommand.vAK,
                "UPHONENUM"     :   mobileno,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTIVATIONCDE" :   activation_code

                ] as [String : Any]
            
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Pin ​Request
        func Validate_Pin_Request(mobileno : String, pin : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict = [
                "SCOMMAND"      :   sCommand.VPIN,
                "UPHONENUM"     :   mobileno,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "UAUTHPIN" :   "111111"
                
                ] as [String : Any]
            
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Self Registration -Fetch questions
        func Fetch_Request(mobileno : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict = [
                "SCOMMAND"      :   sCommand.SR,
                "UPHONENUM"     :   mobileno,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION" :   "FETCH"
                
                ] as [String : Any]
            
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        
        //Self Registration -Submit questions
        func Fetch_Request(data :AnyObject, mobileno:String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict = [
                "SCOMMAND"      :   sCommand.SR,
                "UPHONENUM"     :   mobileno,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION" :   "SUBMIT",
                "QUESTIONS" :   data
                
                ] as [String : Any]
            
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Fetches Account opening data.
        func FetchesAccountopeningdata(mobileno:String,onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict = [
                "SCOMMAND"      :   sCommand.AO,
                "UPHONENUM"     :   mobileno,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION" :   "FETCH",
                 ] as [String : Any]
            
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        
       //  IDValidation (Front and backside image)
        
        func IDValidation(idnumber:String,mobileno:String,backimg:String,frontimg:String,onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            print(sCommand.SR)
            print(mobileno)
            print(sCommand.oRGID)
            print(clientHash)
            print(sCommand.oRIGIN)
            print(idnumber)
            print(appVersion)
            print(UIDevice.current.systemVersion)
            print(UIDevice.current.identifierForVendor?.uuidString)
            let paramDict = [
                "SCOMMAND"      :   sCommand.SR,
                "UPHONENUM"     :   mobileno,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :    UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION" :   "VALIDATE_ID",
                "ID_NUMBER" : idnumber,
                "ID_TYPE" :   "PASSPORT",
                "ID_FRONT" :   frontimg,
                   "ID_BACK" :   backimg,
                   ] as [String : Any] 
            
           /*
             "data:image/jpeg;base64,\(frontimg)",
             "ID_BACK" :  "data:image/jpeg;base64,\(backimg)" ,*/
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        
        //  IDValidation (Front and backside image)
        
        func IDValidationSignature(email:String,kraPIN:String,selfie:String,signature:String,onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            

            let paramDict = [
                "SCOMMAND"      :   sCommand.AO,
                "UPHONENUM"     :   strMobileNumber,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :    UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION" :   "PERSONAL_DETAILS",
                "SELFIE" : selfie,
                "SIGNATURE" :  signature,
                "KRA_PIN" :   kraPIN,
                "EMAIL" :   email,
                ] as [String : Any]
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //  IDValidation (Front and backside image)
        
        func IDValidationFormData(OCCUPATION:Any,NEXT_OF_KIN:Any,RESIDENCE:String,EMAIL:String,BRANCH:String,onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            
            let paramDict = [
                "SCOMMAND"      :   sCommand.AO,
                "UPHONENUM"     :   strMobileNumber,
                "UDEVID"        :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"         :   sCommand.oRGID,
                "CUSTOMER_HASH" :    UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"   :   clientHash,
                "osVersion"     :   UIDevice.current.systemVersion,
                "appVersion"    :   appVersion,
                "ORIGIN"        :   sCommand.oRIGIN,
                "ACTION" :   "SUBMIT",
                "OCCUPATION" : OCCUPATION,
                "NEXT_OF_KIN" :  NEXT_OF_KIN,
                "RESIDENCE" :   RESIDENCE,
                "BRANCH" :   BRANCH,
                "EMAIL" :   EMAIL
                ] as [String : Any]
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Login API
        func login(mobile : String, pin : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let encryptText = encryptPIN(text: pin)
            
            let paramDict = [
                "CLIENT_HASH"       : clientHash,
                "UAUTHPIN"          :   encryptText,
                "ORGID"             :   sCommand.oRGID,
                "UPHONENUM"         :   mobile,
                "ORIGIN"            :   sCommand.oRIGIN,
                "SCOMMAND"          :   sCommand.login,
                "UDEVID"            :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CUSTOMER_HASH"     :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "appVersion"        :   appVersion,
                "osVersion"         :   UIDevice.current.systemVersion
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        ///////////////////////////////////////////    HOME VIEW API'S   ////////////////////////////////////////
        
        //CUP API
        func getCUPData(username : String, onCompletion: @escaping (JSON) -> Void) {
            
           let route = baseURL
           let paramDict =  [
                "CLIENT_HASH"       : clientHash,
                "ORGID"             :   sCommand.oRGID,
                "CUSTOMER_HASH"     :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"         :   username,
                "ORIGIN"            :   sCommand.oRIGIN,
                "SCOMMAND"          :   sCommand.cUP,
                "UDEVID"            :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "appVersion"        :   appVersion,
                "osVersion"         :   UIDevice.current.systemVersion
            ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }

            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                
                onCompletion(json as JSON)
            })
        }
        
        //CUP API for activities
        func getCUPDataForActivites(username : String, onCompletion: @escaping (JSON) -> Void) {
            
            let route = baseURL
            let paramDict =  [
                "CLIENT_HASH"       : clientHash,
                "ORGID"             :   sCommand.oRGID,
                "CUSTOMER_HASH"     :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"         :   username,
                "ORIGIN"            :   sCommand.oRIGIN,
                "SCOMMAND"          :   sCommand.cUP,
                "UDEVID"            :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "appVersion"        :   appVersion,
                "osVersion"         :   UIDevice.current.systemVersion,
                "ALLFLAG"           :   "0"
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Balance Enquiry
        func getBalanceEnquiry(username : String, pin : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "ACCOUNT_ALIAS"     : "Current Account",
                "ALLFLAG"           : "1",
                "UAUTHPIN"          : pin,
                "ORGID"             : sCommand.oRGID,
                "CUSTOMER_HASH"     : UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"       : clientHash,
                "UPHONENUM"         : username,
                "ORIGIN"            : sCommand.oRIGIN,
                "SCOMMAND"          : sCommand.balanceEnqquiry,
                "UDEVID"            : UIDevice.current.identifierForVendor?.uuidString ?? "",
                "appVersion"        :   appVersion,
                "osVersion"         :   UIDevice.current.systemVersion
                ] as [String : Any]
            
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Balance Enquiry
        func getBalanceEnquiryForIndivisualCard(username : String, pin : String, account_alise : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "ACCOUNT_ALIAS"     : account_alise,
                "ALLFLAG"           : "0",
                "UAUTHPIN"          : pin,
                "ORGID"             : sCommand.oRGID,
                "CUSTOMER_HASH"     : UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"       : clientHash,
                "UPHONENUM"         : username,
                "ORIGIN"            : sCommand.oRIGIN,
                "SCOMMAND"          : sCommand.balanceEnqquiry,
                "UDEVID"            : UIDevice.current.identifierForVendor?.uuidString ?? "",
                "appVersion"        :  appVersion,
                "osVersion"         :   UIDevice.current.systemVersion
                ] as [String : Any]
            
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //UPCOMING BILLS
        func getUpcomingBills(username : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "CLIENT_HASH"       : clientHash,
                "ORGID"             : sCommand.oRGID,
                "CUSTOMER_HASH"     : UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"         : username,
                "ORIGIN"            : sCommand.oRIGIN,
                "SCOMMAND"          : sCommand.upcomingBills,
                "UDEVID"            : UIDevice.current.identifierForVendor?.uuidString ?? "",
                "appVersion"        :   appVersion,
                "osVersion"         :   UIDevice.current.systemVersion
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        //REVIEW AND SEND MONEY
        func reviewAndSendMoney(amount : String, account_alias : String, account_no : String, recipent_no : String, username : String, mnoWalletID : String, description : String, recipient_alise : String, nominate_flag : String, apiMethod : String,onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "ACCOUNT_ALIAS"         :   account_alias,
               // "ACCOUNT_NO"            :   account_no,
                "AMOUNT"                :   amount,
                "appVersion"            :   appVersion,
                "CLIENT_HASH"           :   clientHash,
                "NOMINATE_FLAG"         :   nominate_flag,
                "MNO_WALLET_ID"         :   mnoWalletID,
                "osVersion"             :   UIDevice.current.systemVersion,
                "UAUTHPIN"              :   User.password,
                "RECIPIENT_ALIAS"       :   recipient_alise,
                "RECIPIENT_NO"          :   recipent_no,
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"             :   username,
                "ORIGIN"                :   sCommand.oRIGIN,
                "SCOMMAND"              :   apiMethod,
                "UDEVID"                :   UIDevice.current.identifierForVendor?.uuidString ?? ""
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        //PAY BILL
        func payBill(amount : String, account_alias : String, account_no : String, username : String, description : String, aBILLER_REFERENCE : String,aBILLER : String, aENROLL_BILL_REFERENCE : String, aENROLLMENT_ALIAS : String, currency : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "ACCOUNT_ALIAS"         :   account_alias,
                "AMOUNT"                :   amount,
               "appVersion"            :   appVersion,
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "UAUTHPIN"              :   User.password,
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"             :   username,
               "ORIGIN"                :   sCommand.oRIGIN,
                "SCOMMAND"              :   sCommand.pB,
                "UDEVID"                :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "BILLER_REFERENCE"      :  aBILLER_REFERENCE,
                "BILLER"                : aBILLER,
                "ENROLL_BILL_REFERENCE" : aENROLL_BILL_REFERENCE,
                "ENROLLMENT_ALIAS"      : aENROLLMENT_ALIAS,
                "CURRENCY"              : currency
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Internal Transfer
        func internalTransfer(amount : String, account_alias : String, username : String, aRECIPIENT_ACC : String, aRECIPIENT_BANK_BRANCH : String, aNARRATION : String, aRECIPIENT_ALIAS : String, accountNo : String, receipentBank : String, aNOMINATE_FLAG : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "ACCOUNT_ALIAS"         :   account_alias,
                "AMOUNT"                :   amount,
                "appVersion"            :   appVersion,
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "UAUTHPIN"              :   User.password,
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"             :   username,
                "ORIGIN"                :   sCommand.oRIGIN,
                "SCOMMAND"              :   sCommand.tF,
                "UDEVID"                :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "RECIPIENT_ACC"         : aRECIPIENT_ACC,
                "RECIPIENT_BANK_BRANCH" : aRECIPIENT_BANK_BRANCH,
                "NARRATION"             :   aNARRATION,
                "RECIPIENT_ALIAS"       : aRECIPIENT_ALIAS,
                "ACCOUNT_NO"            : accountNo,
                "NOMINATE_FLAG"         :   aNOMINATE_FLAG,
                "RECIPIENT_BANK"        : receipentBank
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        //RTGS Transfer
        func rtgsTransfer(amount : String, account_alias : String, username : String, aRECIPIENT_ACC : String, aRECIPIENT_BANK_BRANCH : String, aRECIPIENT_ACC_NAME : String, aNARRATION : String, aRECIPIENT_BANK : String, aRECIPIENT_ALIAS : String,aNOMINATE_FLAG : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "ACCOUNT_ALIAS"         :   account_alias,
                "AMOUNT"                :   amount,
                "appVersion"            :   appVersion,
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "UAUTHPIN"              :   User.password,
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"             :   username,
                "ORIGIN"                :   sCommand.oRIGIN,
                "SCOMMAND"              :   sCommand.rTGS,
                "UDEVID"                :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "RECIPIENT_ACC"         :   aRECIPIENT_ACC,
                "RECIPIENT_BANK_BRANCH" :   aRECIPIENT_BANK_BRANCH,
                "RECIPIENT_ACC_NAME"    :   aRECIPIENT_ACC_NAME,
                "NARRATION"             :   aNARRATION,
                "RECIPIENT_BANK"        :   aRECIPIENT_BANK,
                "NOMINATE_FLAG"         :   aNOMINATE_FLAG,
                "RECIPIENT_ALIAS"       :   aRECIPIENT_ALIAS
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //RTGS - RTFS Fetch
        func fetchRTGSTime( username : String, account_alias : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"              :   sCommand.FST,
                "UPHONENUM"             :   username,
                "UDEVID"                :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "appVersion"            :   appVersion,
                "ORIGIN"                :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"         :   account_alias
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Card Transfer
        func cardTransfer(amount : String, account_alias : String, username : String, aRECIPIENT_ACC : String, aRECIPIENT_ACC_NAME : String, aNARRATION : String, aCARD_TYPE : String, aNOMINATE_FLAG : String, aRECIPIENT_ALIAS : String, country_code : String, recpt_mobile : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "ACCOUNT_ALIAS"         :   account_alias,
                "AMOUNT"                :   amount,
                "appVersion"            :   appVersion,
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "UAUTHPIN"              :   User.password,
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"             :   username,
                "ORIGIN"                :   sCommand.oRIGIN,
                "SCOMMAND"              :   sCommand.tFC,
                "UDEVID"                :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "RECIPIENT_ACC"         :   aRECIPIENT_ACC,
                "RECIPIENT_ACC_NAME"    :   aRECIPIENT_ACC_NAME,
                "CARD_TYPE"             :   aCARD_TYPE,
                "NOMINATE_FLAG"         :   aNOMINATE_FLAG,
                "NARRATION"             :   aNARRATION,
                "RECIPIENT_ALIAS"       :   aRECIPIENT_ALIAS,
                "COUNTRY_CODE"          :   country_code,
                "RECIPIENT_MOBILE_NUMBER" : recpt_mobile
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //QUERY BILL
        func queryBill(username : String, aBILLER_REFERENCE : String,aBILLER : String, hubService : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
            
                "appVersion"            :   appVersion,
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "UPHONENUM"             :   username,
                "ORIGIN"                :   sCommand.oRIGIN,
                "SCOMMAND"              :   sCommand.qBILL,
                "BILLER_REFERENCE"      :   aBILLER_REFERENCE,
                "BILLER"                :   aBILLER,
                "HUB_SERVICEID"         :  hubService,
                "UDEVID"                : UIDevice.current.identifierForVendor?.uuidString ?? ""
                ] as [String : Any]
            
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
    
        //PESA LINK SECTION---------------------------------------------------------
        //Pesalink - Check Registration
        func pesaCheckRegist( username : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"              :   sCommand.cSR,
                "UPHONENUM"             :   username,
                "UDEVID"                :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                 :   sCommand.oRGID,
                "CUSTOMER_HASH"         :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"           :   clientHash,
                "osVersion"             :   UIDevice.current.systemVersion,
                "appVersion"            :   appVersion,
                "ORIGIN"                :   sCommand.oRIGIN
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Pesalink - Phone lookup
        func pesaPhoneLookup(username : String, aRECIPIENT_MOBILE_NUMBER : String, onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                
                "SCOMMAND"                  :   sCommand.fKITSB,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "RECIPIENT_MOBILE_NUMBER"   : aRECIPIENT_MOBILE_NUMBER
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        ////Pesalink - Send to Phone
        func pesaSendToPhone(amount : String, account_alias : String, username : String,
                             aNOMINATION_ALIAS : String,
                             aBANK_SORT_CODE : String,
                             aRECIPIENT_BANK : String,
                             aNOMINATE : String,
                             aCURRENCY : String,
                             aRECIPIENT_MOBILE_NUMBER : String,
                             aNARRATION : String,
                             BILL_ID : String,
                             FEE_AMOUNT : String,
                             BILL_REFRENCE : String,
                             onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.pAYM,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,

                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "AMOUNT"                    :   amount,
                "NOMINATION_ALIAS"          :   aNOMINATION_ALIAS,
                "BANK_SORT_CODE"            :   aBANK_SORT_CODE,
                "RECIPIENT_BANK"            :   aRECIPIENT_BANK,
                "NOMINATE"                  :   aNOMINATE,
                "CURRENCY"                  :   aCURRENCY,
                "RECIPIENT_MOBILE_NUMBER"   :   aRECIPIENT_MOBILE_NUMBER,
                "NARRATION"                 :   aNARRATION,
                "BILL_ID"                   :   BILL_ID,
                "FEE_AMOUNT"                :   FEE_AMOUNT,
                "BILL_REFRENCE"             :   BILL_REFRENCE

                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        ////Pesalink - Send to Account
        func pesaSendToAcc(amount : String, account_alias : String, username : String,
                             aNOMINATION_ALIAS : String,
                             aBANK : String,
                             aNOMINATE : String,
                             aCURRENCY : String,
                             aRECIPIENT_ACCOUNT_NUMBER : String,
                             aNARRATION : String,
                             onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.pAYAC,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "AMOUNT"                    :   amount,
                "NOMINATION_ALIAS"          :   aNOMINATION_ALIAS,
                "BANK"                      :   aBANK,
                "NOMINATE"                  :   aNOMINATE,
                "CURRENCY"                  :   aCURRENCY,
                "RECIPIENT_ACCOUNT_NUMBER"  :   aRECIPIENT_ACCOUNT_NUMBER,
                "NARRATION"                 :   aNARRATION
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        ////Pesalink - Send to Card
        func pesaSendToCard(amount : String, account_alias : String, username : String,
                           aNOMINATION_ALIAS : String,
                           aNOMINATE : String,
                           aCARD_NUMBER : String,
                           aNARRATION : String,
                           currency : String,
                           onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.pAYC,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "AMOUNT"                    :   amount,
                "NOMINATION_ALIAS"          :   aNOMINATION_ALIAS,
                "NOMINATE"                  :   aNOMINATE,
                "CARD_NUMBER"               :   aCARD_NUMBER,
                "NARRATION"                 :   aNARRATION,
                "CURRENCY"                  :   currency
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        ///Pesalink - Link/Unlink/Primary   -----------------------------------------
        func pesaLinkOrPrimary(account_alias : String, username : String,
                            aACCOUNT_NUMBER : String,
                            aREGISTER_KITS : String,
                            onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.kREG,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                
                "ACCOUNT_ALIAS"             :   account_alias,
                "ACCOUNT_NUMBER"            :   aACCOUNT_NUMBER,
                "UAUTHPIN"                  :   User.password,
                "REGISTER_KITS"             :   aREGISTER_KITS
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        //Pesalink - Paybill Validate Reference
        func pesaValidateReference(username : String,
                               amount : String,
                               aRECIPIENT_MOBILE_NUMBER : String,
                               aRECEIPT_NUMBER : String,
                               onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.pMQUERY,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                
                "RECIPIENT_MOBILE_NUMBER"   :   aRECIPIENT_MOBILE_NUMBER,
                "AMOUNT"                    :   amount,
                "RECEIPT_NUMBER"            :   aRECEIPT_NUMBER
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        // Pesalink ----  End----------------------------
        
        ////More - RequestChequeBook
        func requestChequeBook(leaves : String,
                               account_alias : String,
                               username : String,
                               onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.RCB,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "LEAVES"                    :   leaves,
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        ////More - StopCheque
        func stopCheque(chequeNumber : String,
                               account_alias : String,
                               username : String,
                               reason : String,
                               onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.SC,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "CHEQUE_NO"                 :   chequeNumber,
                "REASON"                    :   reason
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        ////More - ChequeStatus
        func chequeStatus(chequeNumber : String,
                        account_alias : String,
                        username : String,
                        onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.CST,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "CHEQUE_NO"                 :   chequeNumber,
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        ////More - ScanAndPay
        func masterPass(merchantName : String,
                          merchantID : String,
                          extraData : String,
                          account_alias : String,
                          username : String,
                          amount : String,
                          onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.MPB,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "MERCHANT_NAME"             :   merchantName,
                "MERCHANT_ID"               :   merchantID,
                "EXTRADATA"                 :   extraData,
                 "AMOUNT"                   :   amount
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        ////DeleteUpdateBeneficery
        // " ACTION " -- This is the action that the customer has decided "UPDATE" or "DELETE"
        func deleteUpdateBenefieciery(beneficieryAccountType : String,
                        action : String,
                        reciepientAlias : String,
                        username : String,
                        accountNumber : String,
                        onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.beneficiary,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "UAUTHPIN"                  :   User.password,
                "BENEFICIARY_ACCOUNT_TYPE"  :   beneficieryAccountType,
                "ACTION"                    :   action,
                "RECIPIENT_ALIAS"           :   reciepientAlias,
                "ACCOUNT_NUMBER"             :   accountNumber
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
 
        
        ////getAtmAndBranchData
        func atmBranchLocater(lat : String,
                             long : String,
                             flag : String,
                              username : String,
                              onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            
            //let password = self.encryptPIN(text: )
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.BAL,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ATM_BRANCH_FLAG"           :   flag,
                "UAUTHPIN"                  :   User.password,
                "LONGITUDE"                 :   long,
                "LATITUDE"                  :   lat
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        ////Chnage PIN
        func chnagePIN(username : String,
                              aACCOUNT_ALIAS : String,
                              oldPIN : String,
                              aPIN1 : String,
                              aPIN2 : String,
                              onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.changePIN,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   aACCOUNT_ALIAS,
                "UAUTHPIN"                  :   oldPIN,
                "PIN1"                      :   aPIN1,
                "PIN2"                      :   aPIN2
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        ////Chnage Ont Time PIN
        func chnageOTPIN(username : String,
                       aACCOUNT_ALIAS : String,
                       oldPIN : String,
                       aPIN1 : String,
                       aPIN2 : String,
                       onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.changeOntTimePIN,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   aACCOUNT_ALIAS,
                "UAUTHPIN"                  :   oldPIN,
                "PIN1"                      :   aPIN1,
                "PIN2"                      :   aPIN2
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        ////More - MiniStatement
        func miniStatement(account_alias : String,
                        username : String,
                        onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.MSR,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        ////Full Statement
        func fullStatement(account_alias    : String,
                           username         : String,
                           START_DATE       : String,
                           END_DATE         : String,
                           DELIVERY_MODE    : String,
                           EMAIL            : String,
                           onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.FSR,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "START_DATE"                :   START_DATE,
                "END_DATE"                  :   END_DATE,
                "DELIVERY_MODE"             :   DELIVERY_MODE,
                "EMAIL"                     :   EMAIL
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        
        // Phase 2
        ////Safaricom_Mpesa_Paybill // Mpesa Agent Float Purchase
        func safaricom_Mpesa_Paybill(command    : String,
                           account_alias    : String,
                           username         : String,
                           amount           : String,
                           paybillNumber    : String,
                           paybillAccount   : String,
                           enrollAC         : String,
                           enrollAlise      : String,
                           onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict =  [
                "SCOMMAND"                  :   command,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "AMOUNT"                    :   amount,
                "PAYBILL_NUMBER"            :   paybillNumber,
                "PAYBILL_ACCOUNT"           :   paybillAccount,
                "ENROLL_ACCOUNT"            :   enrollAC,
                "ENROLLMENT_ALIAS"          :   enrollAlise
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        ////Safaricom Buy Goods And Services
        func safaricom_Buy_Goods_And_Services(account_alias    : String,
                                     username         : String,
                                     amount           : String,
                                     paybillNumber    : String,
                                     enrollAC         : String,
                                     enrollAlise      : String,
                                     onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.ACCTILL,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "AMOUNT"                    :   amount,
                "PAYBILL_NUMBER"            :   paybillNumber,
                "ENROLL_ACCOUNT"            :   enrollAC,
                "ENROLLMENT_ALIAS"          :   enrollAlise
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
        
        ////Mpesa STK Push
        func mPesaSTKPush(account_alias    : String,
                                              username         : String,
                                              amount           : String,
                                              BRANCH_ID        : String,
                                              ACCOUNT_NUMBER   : String,
                                              onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.STKTOPUP,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "",
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                "AMOUNT"                    :   amount,
                "BRANCH_ID"                 :   BRANCH_ID,
                "ACCOUNT_NUMBER"            :   ACCOUNT_NUMBER
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
    
        
        ////Fixed Deposits
        func fixedDeposits(account_alias    : String,
                          username         : String,
                          onCompletion: @escaping (JSON) -> Void) {
            let route = baseURL
            let paramDict =  [
                "SCOMMAND"                  :   sCommand.FDEPO,
                "UPHONENUM"                 :   username,
                "UDEVID"                    :   UIDevice.current.identifierForVendor?.uuidString ?? "" as Any,
                "ORGID"                     :   sCommand.oRGID,
                "CUSTOMER_HASH"             :   UIDevice.current.identifierForVendor?.uuidString ?? "" as Any,
                "CLIENT_HASH"               :   clientHash,
                "osVersion"                 :   UIDevice.current.systemVersion,
                "appVersion"                :   appVersion,
                "ORIGIN"                    :   sCommand.oRIGIN,
                "ACCOUNT_ALIAS"             :   account_alias,
                "UAUTHPIN"                  :   User.password,
                
                ] as [String : Any]
            
            if Session.sharedInstance.isPrint {
                print(paramDict)
            }
            
            makeHTTPPOSTRequest(route, body: paramDict as [String : AnyObject], onCompletion: { json, err in
                onCompletion(json as JSON)
            })
        }
  
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //MARK: Perform POST Request
        func makeHTTPPOSTRequest(_ path: String, body: [String: AnyObject],onCompletion: @escaping ServiceResponse) {
            var encriptedInfo = [String : AnyObject]()
            
//            if AppMode.appMode == ApplicationMode.development {  //FOR NORMAL REQUEST
//                encriptedInfo = body
//            }
//            else if AppMode.appMode == ApplicationMode.developmentEncpt || AppMode.appMode == ApplicationMode.production { // //FOR ENCRIPTED REQUEST
//                let jsonData = try! JSONSerialization.data(withJSONObject: body, options: [])
//                var decoded = String(data: jsonData, encoding: .utf8) ?? ""
//                decoded = self.encryptPIN(text: decoded)
//                let phnx = Variables().reveal(key: VariableConstants.phnx)
//                let lpg = Variables().reveal(key: VariableConstants.lpg)
//                encriptedInfo[phnx] = "1" as AnyObject
//                encriptedInfo[lpg] = decoded as AnyObject
//            }
            
            let headers = ["content-type": "application/json"]
            let request = NSMutableURLRequest(url: URL(string: path)!)
            request.allHTTPHeaderFields = headers
            request.httpMethod = "Post"
            request.timeoutInterval = 180 // In second
            do {
                // Set the POST body for the request
                let jsonBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                
                request.httpBody = jsonBody
                var session : URLSession!
                if AppMode.appMode == ApplicationMode.production //|| AppMode.appMode == ApplicationMode.developmentEncpt
                {
                    session = URLSession(
                        configuration: URLSessionConfiguration.default,
                        delegate: NSURLSessionPinningDelegate(),
                        delegateQueue: nil)
                }
                else
                {
                    session = URLSession.shared //without pinning
                }
                
                let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                    self.backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
                        self?.endBackgroundTask()
                    }
                    assert(self.backgroundTask.rawValue != 0)
                    
                
                    if let jsonData = data {
                        
                        let json:JSON = JSON(data: jsonData)
                        
                        let phnx = Variables().reveal(key: VariableConstants.phnx)
                        let lpg = Variables().reveal(key: VariableConstants.lpg)
                        
                        if json[phnx].intValue == 1 {
                            
                            let encriptedStr = json[lpg].stringValue
                            let decrptedStr = self.decryptPIN(text: encriptedStr)
                            let decriptedData = Data(decrptedStr.utf8)
                            let json:JSON = JSON(data: decriptedData)
                            
                            if self.isDisableApp(json: json) {
                               return
                            }
                            else {
                                if json == JSON.null {
                                    var jsonTemp = JSON()
                                    jsonTemp["STATUS_CODE"] = 1003
                                    jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                                    onCompletion(jsonTemp, error as NSError?)
                                }
                                else {
                                    onCompletion(json, nil)
                                }
                                
                            }
                        }
                        else {
                            
                            if self.isDisableApp(json: json) {
                                return
                            }
                            else {
                                onCompletion(json, nil)
                            }
                        }
                        
                    } else {
                        
                        var jsonTemp = JSON()
                        jsonTemp["STATUS_CODE"] = 1003
                        jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                        onCompletion(jsonTemp, error as NSError?)
                    }
                })
                
                task.resume()
                
            } catch {
                
                // Create your personal error
                var jsonTemp = JSON()
                jsonTemp["STATUS_CODE"] = 1003
                jsonTemp["STATUS_MESSAGE"] = "Can't perform the requested action due to a temporary server error.\nPlease try again later"
                onCompletion(jsonTemp, error as NSError?)
                
            }
        }
        
        func endBackgroundTask() {
            print("Background task ended.")
            UIApplication.shared.endBackgroundTask(backgroundTask)
            backgroundTask = UIBackgroundTaskIdentifier(rawValue: 0)
        }
        
        ///////////////////////////////Alamofire///////////////////////////////
        
        //ENCRYPT PIN
        func encryptPIN(text : String) -> String {
            
            let input:String = text
            let cipher:String = CryptoHelper.encrypt(input:input) ?? ""
            //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
            //let output:String = CryptoHelper.decrypt(input:cipher)!;
            return cipher
        }
        
        func decryptPIN(text : String) -> String {
            
            let input:String = text
           // let cipher:String = CryptoHelper.encrypt(input:input)!;
            //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
            let output:String = CryptoHelper.decrypt(input:input) ?? "";
            return output
        }
        
        func isDisableApp(json : JSON) -> Bool {

            if json["STATUS_CODE"].intValue == 211 {
                
                DispatchQueue.main.async {
                    let visibleViewController = self.getVisibleViewController(nil)
                    visibleViewController?.showMessage(title: "Alert!", message: json["STATUS_MESSAGE"].stringValue)
                }
                return true
            }
            return false
        }
        
        func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
            
            var rootVC = rootViewController
            if rootVC == nil {
                rootVC = UIApplication.shared.keyWindow?.rootViewController
            }
            
            if rootVC?.presentedViewController == nil {
                return rootVC
            }
            
            if let presented = rootVC?.presentedViewController {
                if presented.isKind(of: UINavigationController.self) {
                    let navigationController = presented as! UINavigationController
                    return navigationController.viewControllers.last!
                }
                
                if presented.isKind(of: UITabBarController.self) {
                    let tabBarController = presented as! UITabBarController
                    return tabBarController.selectedViewController!
                }
                
                return getVisibleViewController(presented)
            }
            return nil
        }
    }

