//
//  AppDelegate.swift
//  Stanbic_bank
//
//  Created by 5Exceptions6 on 10/04/19.
//  Copyright © 2019 5Exceptions. All rights reserved.
//

import UIKit
import CoreData
import EncryptedCoreData


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appLanguage = "en"


    private func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
       // AppMode.appMode = ApplicationMode.production
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
      
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
       
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
      
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
       }

    func applicationWillTerminate(_ application: UIApplication) {

    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "Model")
        
        var options = [EncryptedStorePassphraseKey: Variables().reveal(key: VariableConstants.VarialbeString1), EncryptedStoreFileManagerOption: EncryptedStoreFileManager.default(), NSMigratePersistentStoresAutomaticallyOption : true, NSInferMappingModelAutomaticallyOption : true] as [String : Any]
        
        do {
            let description: NSPersistentStoreDescription? = try EncryptedStore.makeDescription(options: options, configuration: nil)
            
            container.persistentStoreDescriptions = [description] as! [NSPersistentStoreDescription]
            
            container.loadPersistentStores(completionHandler: { description, error in
                if error != nil {
                    if let anError = error {
                        //print("error! \(anError)")
                    }
                }
            })
        } catch let error as NSError {
            // print("Error")
        }
        
        //        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
        //            if let error = error as NSError? {
        //
        //                fatalError("Unresolved error \(error), \(error.userInfo)")
        //            }
        //        })
        return container
    }()
    
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

