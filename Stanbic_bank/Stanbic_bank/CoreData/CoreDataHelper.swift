//
//  CoreDataHelper.swift
//  DTB
//
//  Created by Five Exceptions on 06/09/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataHelper : NSObject {
    
    var context : NSManagedObjectContext? = nil
    
    override init() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
    }
    
    func saveDataFor(json : JSON, entityName : String) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        self.removeAllRecords(entityName)
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        
        for i in 0 ..< (json[entityName].count ) {
            
            let account_info = NSManagedObject(entity: entity!, insertInto: context)
            
            let account_data = json[entityName][i]
            
            if entityName == "RESULT_ARRAY" {
                let hasValidKey =  account_info.entity.propertiesByName.keys.contains("aCIF")
                if hasValidKey == true {
                    account_info.setValue(json["CIF"].stringValue, forKey: "aCIF")
                }
            }
            
            
            for (key, value) in account_data {
                let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
                if hasValidKey == true {
                    account_info.setValue(value.stringValue, forKey: "a"+key)
                }
            }
        }
        saveContext()
    }
    

    func saveDataForACInfo(json : JSON, entityName : String) {

        //FIRST REMOVE ALL RECORDS FROM ENTITY
        self.removeAllRecords(entityName)
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        
        var index = 0
        
        for i in 0 ..< (json[entityName].count ) {
            
            
            let account_info = NSManagedObject(entity: entity!, insertInto: context)
            
            //let eValue1 = convertValue(text: "\(index)")
            account_info.setValue("\(index)", forKey: "srNumber")
            
            let account_data = json[entityName][i]
            for (key, value) in account_data {
                let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
                if hasValidKey == true {
                    account_info.setValue(value.stringValue, forKey: "a"+key)
                }
            }
            
            index += 1
        }

        saveContext()
        
    }
    
    func saveDataForActiveServices(json : JSON, entityName : String, keyName : String) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        self.removeAllRecords(entityName)
        
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        
        for i in 0 ..< (json[keyName].count ) {
            let account_info = NSManagedObject(entity: entity!, insertInto: context)
            let account_data = json[keyName][i]
            for (key, value) in account_data {
                let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
                if hasValidKey == true {
                    account_info.setValue(value.description, forKey: "a"+key)
                }
            }
        }
        
        saveContext()
        
    }
    func saveRegex(value : String, entityName : String, key : String, removeRecords : Bool) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        if removeRecords == true {
            self.removeAllRecords(entityName)
        }
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        let account_info = NSManagedObject(entity: entity!, insertInto: context)
        
        let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
        if hasValidKey == true {
            account_info.setValue(value, forKey: "a"+key)
        }
        saveContext()
    }
    
    func saveUUID(value : String, entityName : String, key : String, removeRecords : Bool) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        if removeRecords == true {
            self.removeAllRecords(entityName)
        }
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        let account_info = NSManagedObject(entity: entity!, insertInto: context)
        
        let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
        if hasValidKey == true {
            account_info.setValue(value, forKey: "a"+key)
        }
        saveContext()
    }

    
    func saveDataForDictionary(json : JSON, entityName : String, key : String, removeRecords : Bool) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        if removeRecords == true {
            self.removeAllRecords(entityName)
        }
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        let account_info = NSManagedObject(entity: entity!, insertInto: context)
        
        for (key, value) in json {
            let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
            if hasValidKey == true {
                account_info.setValue(value.stringValue, forKey: "a"+key)
            }
        }
        saveContext()
    }
    
    func saveDataForDictionaryBE(json : JSON, entityName : String, key : String, removeRecords : Bool, index : Int) {
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        if removeRecords == true {
           self.removeAllRecords(entityName)
        }
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        let account_info = NSManagedObject(entity: entity!, insertInto: context)
       
       // let eValue1 = convertValue(text: "\(index)")
        account_info.setValue("\(index)", forKey: "srNumber")
        
        for (key, value) in json {
            let hasValidKey =  account_info.entity.propertiesByName.keys.contains("a"+key)
            if hasValidKey == true {
                account_info.setValue(value.stringValue, forKey: "a"+key)
            }
        }
        saveContext()
    }
    
    func updateDataForDictionaryBE(beArray : [[String : String]], entityName : String, removeRecords : Bool) {
        
        
        //FIRST REMOVE ALL RECORDS FROM ENTITY
        self.removeAllRecords(entityName)
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: context!)
        
        for i in 0 ..< (beArray.count) {
            
            let account_info = NSManagedObject(entity: entity!, insertInto: context)
            
            let account_data = beArray[i]
            
            
            for (key, value) in account_data {
                let hasValidKey =  account_info.entity.propertiesByName.keys.contains(key)
                if hasValidKey == true {
                    account_info.setValue(value, forKey: key)
                }
            }
        }
        
        saveContext()
    }
    

    func getDataForEntity(entity : String) -> [[String : String]] {

        let arr_all_keys  = CoreDataKey().getCoredataKeyForEntity(entity: entity)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        var data_array = [[String : String]]()

        request.returnsObjectsAsFaults = false
        do {
            let result = try context?.fetch(request)
            for data in result as! [NSManagedObject] {
                 
                var dict_data = [String : String]()
                for i in 0 ..< arr_all_keys.count {
                    
                    //let value = self.convertValue1(text: data.value(forKey: arr_all_keys[i]) as! String)
                    dict_data[arr_all_keys[i]] = data.value(forKey: arr_all_keys[i]) as? String
                    
                }
                data_array.append(dict_data)
            }

        } catch {

        }
        return data_array
    }
    

    func removeAllRecords(_ entityName: String) -> Void {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        var results: [AnyObject] = []
        do {
            results = (try context?.fetch(request))!
        } catch _ as NSError {
           
        }
        
        for record in results {
            context?.delete(record as! NSManagedObject)
        }
        saveContext()
    }
    
    func removeParticularRecord(_ entityName: String,Biller_refrence:String) -> Void {
        
        let arr_all_keys  = CoreDataKey().getCoredataKeyForEntity(entity: entityName)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        var data_array = [[String : String]]()
        
        //request.returnsObjectsAsFaults = false
        do {
            let result = try context?.fetch(request)
            for data in result as! [NSManagedObject] {
                
                var dict_data = [String : String]()
                for i in 0 ..< arr_all_keys.count {
                    
                    //let value = self.convertValue1(text: data.value(forKey: arr_all_keys[i]) as! String)
                    dict_data[arr_all_keys[i]] = data.value(forKey: arr_all_keys[i]) as? String
                    
                }
                data_array.append(dict_data)
                if dict_data["aBILL_REFERENCE"] == Biller_refrence
                {
                    context?.delete(data )
                }
            }
            
        } catch {
            
        }
        saveContext()

    }
    
    
    func saveContext() -> Void {
        
        do {
            try context?.save()
            
        } catch _ as NSError {
           
        }
    }
    
    func convertValue(text : String) -> String {
        
        let input:String = text
        let cipher:String = CryptoHelper.encrypt(input:input)!;
        //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
        //let output:String = CryptoHelper.decrypt(input:cipher)!;
        return cipher
    }
    
    func convertValue1(text : String) -> String {
        
        let input:String = text
        //let cipher:String = CryptoHelper.encrypt(input:input)!;
        //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
        let output:String = CryptoHelper.decrypt(input:input)!;
        return output
    }
    
}
