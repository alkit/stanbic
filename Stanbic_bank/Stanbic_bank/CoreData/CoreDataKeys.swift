//
//  CoreDataKeys.swift
//  DTB
//
//  Created by Five Exceptions on 06/09/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import Foundation


class CoreDataKey : NSObject {
    
    func getCoredataKeyForEntity(entity : String) -> [String] {
        
        switch entity {
            
        case "ACCOUNTS_INFO":
            return ["aCURRENCY_CODE", "aPROFILE_ID", "aACCOUNT_ALIAS", "aCURRENCY_NUMBER", "aACCOUNT_NUMBER", "aTARIFF_ID", "aCURRENCY", "aACCOUNT_ID", "aACCOUNT_TYPE", "srNumber"]
            
        case "PROFILES_INFO":
            return ["aPROFILE_ID", "aPIN_STATUS", "aFIRST_NAME", "aOTHER_NAMES", "aLAST_LOGIN", "aLAST_PIN_CHANGE"]
            
        case "MONEY_TRANSFER_INFO":
            return ["aMONEY_TRANSFER_PROVIDER", "aMONEY_TRANSFER_CODE", "aMTCN_LENGTH"]
            
        case "BANKS_INFO":
            return ["aBANK_ID", "aBANK", "aBANK_CODE", "aDATE"]
            
        case "BANK_BRANCHES_INFO":
            return ["aBANK_ID", "aBANK_BRANCH", "aBRANCH_CODE", "aDATE"]
            
        case "CURRENCIES_INFO":
            return ["aCURRENCY", "aCURRENCY_CODE", "aCURRENCY_NUMBER"]
            
        case "MNO_INFO":
            return ["aMNO_NAME", "aMNO_CODE", "aMNO_WALLET", "aMNO_WALLET_ID", "aORDER_ID", "aACTIVE", "aDATE", "aINACTIVE_MESSAGE"]
            
        case "BILLERS_INFO":
            return ["aBILLER_NAME", "aBILLER", "aBILLER_REFERENCE", "aENROLLMENT_ALIAS", "aACTIVE", "aDATE", "aBILLER_LOGO", "aPRESENTMENT_TYPE", "aREFERENCE_LABEL", "aHUB_SERVICEID", "aINACTIVE_MESSAGE"]
            
        case "MOBILE_MONEY_INFO":
            return ["aMNO_NAME", "aMNO_CODE", "aMNO_WALLET", "aMNO_WALLET_ID", "aORDER_ID", "aACTIVE", "aDATE", "aINACTIVE_MESSAGE"]
            
        case "CONTACT_INFO":
            return ["aCONTACT_TITLE", "aLOCATION", "aPHONE_FAX", "aADDRESS", "aEMAIL", "aDATE", "aTWITTER", "aFACEBOOK", "aWEBSITE", "aLINKEDIN", "aYOUTUBE"]
            
            
        case "ActiveServices":
            return ["aSERVICE_NAME", "aSERVICE_CODE", "aACTIVE", "aINACTIVE_MESSAGE", "aCHARGE"]
            
            
        case "ENROLLMENTS_INFO":
            return ["aENROLLMENT_ALIAS", "aBILLER", "aBILLER_REFERENCE", "aBILLER_NAME", "aBENEFICIARY_TYPE", "aMNO", "aMNO_WALLET_ID", "aDATE"]
            
        case "BALANCE_ENQUIRY_STATUS_MESSAGE":
            return ["aSTATUS_MESSAGE"]
            
        case "BALANCE_ENQUIRY":
            return ["aACCOUNT_ALIAS", "aBALANCE", "aCURRENCY", "aDATE", "aMESSAGE", "srNumber"]
            
        case "FETCHED_BILLS":
            return ["aAMOUNT", "aBILL_DESCRIPTION", "aBILL_REFERENCE", "aBILLER", "aBILLER_LOGO", "aBILLER_NAME", "aCUSTOMER_NAME", "aDUE_DATE", "aLAST_CHECKED"]
            
        case "BENEFICIARIES_INFO":
            return ["aNOMINATION_ALIAS", "aNOMINATED_ACCOUNT", "aBANK_BRANCH_NAME", "aBANK_NAME", "aBANK_CODE", "aBRANCH_CODE", "aBENEFICIARY_TYPE", "aDATE"]
            
        case "CARD_INFO":
            return ["aCARD_ID", "aCARD_NAME", "aCARD_TYPE"]
            
        case "MARKETING_INFO":
            return ["aHEADER", "aM_IMAGE", "aM_TEXT", "aMID", "aM_IMAGE_URL", "aM_CONTACT"]
            
        case "RESULT_ARRAY":
            return ["aDESCRIPTION", "aAMOUNT", "aBOOKING_DATE", "aMATURITY_DATE", "aRATE", "aREFERENCE", "aCURRENCY", "aCIF"]
            
        case "PIN_REGEX":
            return ["aPIN_REGEX"]
            
        case "BALANCE_EXPIRE_TIME":
            return ["aBALANCE_EXPIRE_TIME"]
            
        case "DEVICEID":
            return ["aDEVICEID"]
            
        default:
            return [""]
        }
    }
    
}

