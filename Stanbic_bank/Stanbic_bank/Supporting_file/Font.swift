//
//  Font.swift
//  DTB
//
//  Created by Five Exceptions on 10/09/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import Foundation
import  UIKit

class Fonts {
    
    // SourceSansPro
//    static let kFontRegular =  "SourceSansPro-Regular"
//    static let kFontBold =  "SourceSansPro-Bold"
//    static let kFontSemibold = "SourceSansPro-Semibold"
    
//    SFProDisplay-Regular
//    SFProDisplay-Bold
//    SFProDisplay-Semibold
//    SFProDisplay-Medium
    
    
    
    static let kFontRegular =  "SFProDisplay-Regular"
    static let kFontBold =  "SFProDisplay-Bold"
    static let kFontSemibold = "SFProDisplay-Semibold"
    static let kFontMedium = "SFProDisplay-Medium"
    static let kFontVarela = "SFProDisplay-Medium"
    
    func set(object : NSObject, fontType : Int, fontSize : Int, color : UIColor, title : String,placeHolder:String) {
        
        //FOR LOCALIZATION
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let appLanguage = appDelegate.appLanguage
        let localTitle = title
        let placeholder = placeHolder
        
        switch object
        {
        case (object as UILabel) :
            (object as! UILabel).font = UIFont.init(name: getFontString(type: fontType), size: CGFloat(fontSize))
            (object as! UILabel).textColor = color
            (object as! UILabel).text = localTitle
            break
            
        case (object as UIButton) :
            (object as! UIButton).titleLabel?.font = UIFont.init(name: getFontString(type: fontType), size: CGFloat(fontSize))
            (object as! UIButton).setTitleColor(color, for: .normal)
            (object as! UIButton).setTitle(localTitle, for: .normal)
            break
    
        case (object as UITextField) :
            // Changed by Vijay
            (object as! UITextField).font = UIFont.init(name: getFontString(type: fontType), size: CGFloat(fontSize))
            (object as! UITextField).textColor = color
            (object as! UITextField).placeholder = placeholder
            break
            
            
        default:
            break
        }
        
    }
    
    
    func getFontString(type : Int) -> String
    {
        
        switch type {
            
        case 0:
            return Fonts.kFontRegular
            
        case 1:
             return Fonts.kFontMedium
            
        case 2:
            return Fonts.kFontBold
            
        case 3:
            return Fonts.kFontSemibold
            
        default:
            return Fonts.kFontRegular
        }
        
 }
}
