//
//  Variables.swift
//  DTB
//
//  Created by Five Exceptions on 11/10/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import Foundation

class Variables {
    // MARK: - Variables

    private var slt: String
    
    // MARK: - Initialization
    
    init() {
        self.slt = "\(String(describing: AppDelegate.self))\(String(describing: NSObject.self))"
    }
    
    init(with salt: String) {
        self.slt = salt
    }
    
    func reveal(key: [UInt8]) -> String {
        let sifer = [UInt8](self.slt.utf8)
        let length = sifer.count
        
        var dcpt = [UInt8]()
        
        for k in key.enumerated() {
            dcpt.append(k.element ^ sifer[k.offset % length])
        }
        
        return String(bytes: dcpt, encoding: .utf8)!
    }
}

enum VariableConstants {
    static let VarialbeString: [UInt8] = [52, 27, 24, 116, 7, 6, 1, 80, 2, 7, 31, 38, 42, 127, 12, 11]
    
    static let VarialbeString1: [UInt8] = [24, 63, 37, 5, 55, 41, 34, 34, 47, 49, 44, 1, 6, 28]
    
    static let phnx: [UInt8] = [17, 56, 63, 1, 43, 37, 61]
    
    static let lpg: [UInt8] = [17, 60, 52]
}
