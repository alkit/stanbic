//
//  constant.swift
//  iDDNA
//
//  Created by Vijay Patidar on 21/06/17.
//  Copyright © 2017 5Exceptions. All rights reserved.
//

import Foundation
import UIKit

// Color Constant
class sCommand {
    
    static let oRGID = AppMode.appMode?.getOriginID() ?? 2
    static let oRIGIN =  "5EXCEPTIONS"
    
    static let MSR        = "689L9GE"   //Mini Statement
    static let gAK          = "PBXUDGP" //Get validation Key
    static let vAK          = "Z0U7021"  //Validate Activation Key
    static let VPIN          = "SIU6GM9"  //Validate Pin Request
    static let SR          = "YXZI4EU"  //SelF REgisration
    static let AO         = "9XTY5RE"  // Account Opening
  
    
    static let CUP             = "5Z3Q32L"  // Check Updates/Fetch System Settings
    static let login        = "SIU6GM9"
    static let cUP          = "5Z3Q32L" //Check Updates/Fetch System Settings
    static let balanceEnqquiry          = "OTFUFMD" //Balance Enquiry
    static let upcomingBills         = "IGHIK41"
    static let sendMoney     = "8C0G7J8" //Send money to mobile
    static let bAO = "R64ZN7G" //Buy Airtime
    static let pB = "KYTJT5F"  //Pay Bill
    static let qBILL = "FR4GX9T"  //Query Bill
    static let tF = "DSZFQUU"  //Internal Transfer
    static let tFC = "VN7CW1R"  //Card Transfer
    static let rTGS = "4MK2LKD"  //RTGS Transfer
    static let FST = "YR1D7VT"  //RTGS Fetch
    
    //PESA LINK------------------------------------
    static let cSR = "9OTOU88"  //Pesalink - Check Registration
    static let fKITSB = "ET5353GE"  //Pesalink - Phone lookup
    static let pAYM = "L172R9Z"  //Pesalink - Send to Phone
    static let pAYAC = "4YLDLII"  //Pesalink - Send to Account
    static let pAYC = "RUWQC5H"  //Pesalink - Send to Card
    static let kREG = "RNLYW14"  //Pesalink - Link/Unlink/Primary
    static let pMQUERY = "FR8HJOP"  //Pesalink - Paybill Validate Reference
    
    //More
    static let RCB = "3869V69"      //Check Book Request
    static let SC = "KXEMBDH"      //Check Book Request
    static let CST = "3FQ9ROE"      //Check Book Request
    static let BAL = "SVZDQ0O"      //ATMBranchlocater
    
    //Scan and Pay
    static let MPB = "FJ373HDH"    //Scan and Pay   DLZ1MYO
    
    //Delete and Update Beneficiary
    static let beneficiary = "DLZ1MYO"
    
    //Chnage PIN
    static let changePIN = "UX7UF7I"    //CP - Chnage PIN
    static let changeOntTimePIN = "JHSWF2Y"    //COTP - chnageOntTimePIN
    static let FSR = "7AKFXH0"    //Full statement
    
    //Ph 2
    static let ACCBILL = "QXQQ3MX"    //Safaricom Mpesa Paybill
    static let ACCTILL = "CQJ26ST"    //Mpesa Business Buy Goods and Services
    static let STKTOPUP = "GQU2W3A"    //Mpesa STK Push
    static let SFCAGENT = "TKG2ATD"    //Mpesa Agent Float Purchase
    static let FDEPO = "F5MXK78"    //Fixed Deposits
    
    
}

struct KeychainConfiguration
{
    static let kLastAccessedUsername  = "lastAccessedUsername"
    static let kLastAccessedPassword  = "lastAccessedPassword"
    static let kIsAuthEnable          = "isAuthEnable"
    static let kIsDataSavedInCoreData          = "isDataSavedInCoreData "
    static let kUserContry                  = "kUserContry"
    static let accessGroup: String? = nil
    //NOTIFICATION OBSERVATION
    static let notificationID = "applicationDidBecomeActive"
    static let updatedBE = "BEUpdationNotification"
    static let updatedBENEW = "BEUpdationNotificationNEW"
    static let beFail = "BEFailNotification"
    static let paymentSuccess = "PAYMENTSUCCESS"
    static let callBENEW = "CALLBENEW"
    static let encptKey = "ENCPTIONSKEY"
    static let encptAccount = "ENCPTIONSACCOUNT"
    static let encptValue = "ENCPTIONSACCOUNT"
    static let miniStatement = "MINISTATEMENT"
}

class colorConstant
{
   
    static let kBlackFont: UIColor = UIColor(red: 51/255.0, green: 51/255.0, blue: 51/255.0, alpha: 1.0)
    static let kAppThemeColor: UIColor = UIColor(red: 231/255.0, green: 27/255.0, blue: 61/255.0, alpha: 1.0)
    static let kRedColor: UIColor = UIColor(red: 255.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0) // //ff0000 //e71b3d
    static let kRedTitleColor: UIColor = UIColor(red: 231.0/255.0, green: 27/255.0, blue: 61/255.0, alpha: 1.0)
    static let kTransparentRedColor: UIColor = UIColor(red: 255.0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.1) // //ff0000
    static let kYellowColor: UIColor = UIColor(red: 250.0/255.0, green: 164/255.0, blue: 49/255.0, alpha: 1.0) //faa431
    static let kTransparentYellowColor: UIColor = UIColor(red: 0/255.0, green: 161/255.0, blue: 224/255.0, alpha: 0.1)
    static let kBGColor: UIColor = UIColor(red: 0/255.0, green: 161/255.0, blue: 224/255.0, alpha: 1.0)
    static let kLightGrayColor: UIColor = UIColor(red: 155.0/255.0, green: 155.0/255.0, blue: 155.0/255.0, alpha: 1.0) // 9b9b9b
    static let kDarkGrayColor: UIColor = UIColor(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0)//808080
    static let kWhiteColor: UIColor = UIColor.white
    static let kBlackColor: UIColor = UIColor.black
    static let kGrayColor:UIColor = UIColor(red: 115/255.0, green: 118/255.0, blue: 127/255.0, alpha: 1.0)//73767f
    static let kCyanColor:UIColor = UIColor(red: 192/255.0, green: 15/255.0, blue: 44/255.0, alpha: 1.0)//c20f2c
    static let kLightBluColor:UIColor = UIColor(red: 0/255.0, green: 122/255.0, blue: 255/255.0, alpha: 1.0)//007AFF
    static let kdarkGrayColor:UIColor = UIColor(red: 74/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1.0) //4a4a4a
    static let kDrakRedColor: UIColor = UIColor(red: 194/255.0, green: 15/255.0, blue: 44/255.0, alpha: 1.0) //c20f2c
    static let k8e8e93: UIColor = UIColor(red: 142/255.0, green: 142/255.0, blue: 147/255.0, alpha: 1.0) //8e8e93
    static let k171717: UIColor = UIColor(red: 23/255.0, green: 23/255.0, blue: 23/255.0, alpha: 1.0) //171717 Dark Text color
    static let btnGraycolor: UIColor = UIColor(red: 195/255.0, green: 195/255.0, blue: 195/255.0, alpha: 1.0)
    static let kF2F2F2 : UIColor = UIColor(white: 242/255, alpha: 1)
    static let ececec : UIColor = UIColor(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1.0) 
    static let kGrayDisableColor = UIColor(red: 195/255, green: 195/255, blue: 195/255, alpha: 1)
    static let k333333 = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    static let k4e4e4e = UIColor(red: 78/255, green: 78/255, blue: 78/255, alpha: 1) // 4e4e4e
}


enum ApplicationMode : Int {
    case development = 0, production, developmentEncpt
    
   
    func getBaseUrl() -> String {
        switch self {

        case .development:  //Base Url development
            let encryptedURL = "JsO1GV/nVos64DImGEU0NVeSdq/jewOC8uE0aWpPNNjle53F04pIcQ5EWWFa8pbv"
            let output:String = CryptoHelper.decrypt(input:encryptedURL) ?? "";
            return output
            
            
        case .production:  //Base Url Production
            let encryptedURL = "djP5n7QfVW//my+zul2+d5gS9g9w01cgBZ7YSzFL1ZAr13BT6Xf0NjiHAf29QY4qw51bDwzKmx5MxVIV/6iGEw=="
            let output:String = CryptoHelper.decrypt(input:encryptedURL) ?? "";
            return output
            
            
        case .developmentEncpt : //Base Url Encryption
            let encryptedURL = "JsO1GV/nVos64DImGEU0NVeSdq/jewOC8uE0aWpPNNjMAtxdjv1qNAVJtQruZfPWB9uPTBPHhAP0AGL30rEpSg=="
            let output:String = CryptoHelper.decrypt(input:encryptedURL) ?? "";
            return output
            
        }
    }
    
    
    
    func getOriginID() -> Int {
        // FOR DEVELOPMENT MODE 13 // FOR PRODUCTION MODE 7
//        switch self {
//        case .development, .developmentEncpt:
//            return 13
//
//        case .production:  //Base Url Production
//            return 7
//        }
        
        return 2
    }
//    func dcptPIN(text : String) -> String {
//
//        let input:String = text
//        //let cipher:String = CryptoHelper.encrypt(input:input)!;
//        //cipher = "kb1TyFRUcMaY6Z1vCRravA==";
//        let output:String = CryptoHelper.decrypt(input:input) ?? "";
//        return output
//    }
}

struct AppMode {
    static var appMode : ApplicationMode?
}

enum FlowType : Int {
    
    case sendMoney = 0, buyAirtime, payBill, internalTransfer, rtgs, cardTransfer, safaricomPaybill, safariconBuyGoods,   agentFloatPurchase, mPesaSTKPush, pesaLink, sentToPhone, sentToAccount, sentToCard,scanToPay, termsAndCond, knowMoreButton, helpSupport, chequeBookRequest, stopCheque, chequeStatus
    
    func titleHeader() -> String {
        switch self {
            
        case .sendMoney, .pesaLink, .sentToPhone, .sentToAccount, .sentToCard:
            return "Review and Send"
            
        case .buyAirtime:
            return "Review and Buy"
            
        case .payBill,.scanToPay :
            return "Review and Pay"
            
        case .internalTransfer, .rtgs, .cardTransfer, .safaricomPaybill, .safariconBuyGoods, .agentFloatPurchase, .mPesaSTKPush :
            return "Review and Transfer"
            
        case .termsAndCond, .knowMoreButton, .helpSupport, .chequeBookRequest, .stopCheque, .chequeStatus:
            return ""
        }
    }
    
    func buttonTitle() -> String {
        switch self {
            
        case .sendMoney, .pesaLink, .sentToPhone, .sentToAccount, .sentToCard,.scanToPay :
            return "Send Money"
            
        case .payBill:
            return "Pay Bill"
            
        case .internalTransfer, .rtgs, .cardTransfer, .safaricomPaybill, .safariconBuyGoods, .agentFloatPurchase, .mPesaSTKPush :
            return "Transfer Funds"
            
        case .buyAirtime:
            return "Buy Airtime"
            
        case .termsAndCond, .knowMoreButton, .helpSupport, .chequeBookRequest, .stopCheque, .chequeStatus:
            return ""
        
            
        }
    }
    
    func transactionType() -> String {
        switch self {
            
        case  .payBill :
            return "Pay Bill"
            
        case .internalTransfer, .rtgs, .cardTransfer :
            return "Funds Transfer"
            
        case .safaricomPaybill, .safariconBuyGoods, .agentFloatPurchase, .mPesaSTKPush :
            return "Mobile Money"
            
        case .pesaLink, .sentToPhone, .sentToAccount, .sentToCard :
            return "Pesa Link"
            
        case .scanToPay :
            return "Masterpass"  // as zeplin
            
        case .sendMoney :
            return "Send Money"
            
        case .buyAirtime :
            return "Buy Airtime"
            
        case .termsAndCond, .knowMoreButton, .helpSupport, .chequeBookRequest, .stopCheque, .chequeStatus:
            return ""
        
        }
    }
}

struct Flow {
    
    static var flowType : FlowType?
}
struct User {
    
    static var username  = ""
    static var password = ""
}

extension String {
    var pairs: [String] {
        var result: [String] = []
        let characters = Array(self)
        stride(from: 0, to: count, by: 2).forEach {
            result.append(String(characters[$0..<min($0+2, count)]))
        }
        return result
    }
    mutating func insert(separator: String, every n: Int) {
        self = inserting(separator: separator, every: n)
    }
    func inserting(separator: String, every n: Int) -> String {
        var result: String = ""
        let characters = Array(self)
        stride(from: 0, to: count, by: n).forEach {
            result += String(characters[$0..<min($0+n, count)])
            if $0+n < count {
                result += separator
            }
        }
        return result
    }
    
//    //get formatted currency
//    func getCurrency()-> String?{
//
//        let number = Double(self)
//        if number == nil{
//            return ""
//        }
//        let price = NSNumber(value: number!)
//
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .currency
//        formatter.currencySymbol = " "
//        //formatter.usesGroupingSeparator = true
//        //formatter.groupingSeparator = ","
//
//        formatter.locale = Locale(identifier: "en_US")
//        var priceStr = formatter.string(from: price)
//        if priceStr == nil{
//            return ""
//        }
//        //var _ = priceStr?.removeFirst()
//        //priceStr = priceStr?.replacingOccurrences(of: ".", with: ",")
//        return  priceStr // $123"
//    }
}


class CustomNavController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent;
    }
}


extension String
{
    func getCurrency(amountStr: String)-> String?{

        let number = Double(amountStr)

        if number == nil{
            return ""
        }
        let price = NSNumber(value: number!)

        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = " "

        formatter.locale = Locale(identifier: "en_US")
        var priceStr = formatter.string(from: price)
        if priceStr == nil{
            return ""
        }
        priceStr = priceStr?.replacingOccurrences(of: " ", with: "")
        //priceStr?.removeFirst()
        //priceStr = priceStr?.replacingOccurrences(of: ".", with: ",")
        return  priceStr // $123"
    }
    
    
    // formatting text for currency textField
    func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = " "
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        
        return formatter.string(from: number)!
    }
}

extension UIViewController {
    
    // Mark:- Custom Methods
    // Safaricom,Airtel, Airtelmoney, Telkom, M-Pesa

    
    func getActiveServiceCode(service : String) -> String {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let appLanguage = appDelegate.appLanguage
        
        switch service {
            
        case  ("send_Money") :
            return "SM"
            
        case  ("Buy_Airtime") :
            return "BAO"
            
        case  ("Scan_to_Pay")  :
            return "MPB"
            
        case  ("Pay_bills")  :
            return "PB"
            
        case  ("internal")  :
            return "TF"
            
        case  ("Rtgs")  :
            return "RTGS"
            
        case  ("card")  :
            return "TFC"
            
        case  ("safaricomPaybill")  :
            return "ACCBILL"
            
        case  ("safaricomBuy")  :
            return "ACCTILL"
            
        case  ("AgentPurchase")  :
            return "SFCAGENT"
            
        case  ("Send to Phone")  :
            return "PAYM"
            
        case  ("Send to Account")  :
            return "PAYAC"
            
        case  ("Send to Card")  :
            return "PAYC"
            
        case  ("Link your Account")  :
            return "KREG"
            
        case  ("Unlink your Account")  :
            return "KREG"
            
        case  ("Primary Account")  :
            return "KREG"
            
        case  ("Export")  :
            return "FSR"
            
        case  ("LOADACCOUNTMPESA")  :
            return "STKTOPUP"
            
        default:
            return ""
        }
    }
    
    //ACCBILL,ACCTILL, SFCAGENT
    
    func checkActiveService(service : String) -> String {
        
        let serviceCode = self.getActiveServiceCode(service: service)
        let activeService = CoreDataHelper().getDataForEntity(entity : "ActiveServices")
        
        for i in 0 ..< activeService.count {
            if activeService[i]["aSERVICE_CODE"] == serviceCode {
                return activeService[i]["aACTIVE"] ?? ""
            }
        }
        return ""
    }
    
    func getInActiveMessage(service : String) -> String {
        
        let serviceCode = self.getActiveServiceCode(service: service)
        let activeService = CoreDataHelper().getDataForEntity(entity : "ActiveServices")
        for i in 0 ..< activeService.count {
            
            if activeService[i]["aSERVICE_CODE"] == serviceCode {
                
                if activeService[i]["aACTIVE"] == "2" || activeService[i]["aACTIVE"] == "3" { // checking for three is only for send money
                    return activeService[i]["aINACTIVE_MESSAGE"] ?? ""
                }
                else {
                    return ""
                }
            }
        }
        return ""
    }
    
    func showMessage(title : String, message : String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
//            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
//        })
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) { action in
            
        })
        present(alert, animated: true)
    }
    
    func showMessageBack(title : String, message : String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            
            self.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true)
    }
    
    
    
}

