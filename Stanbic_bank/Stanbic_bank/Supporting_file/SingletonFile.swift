//
//  SingletonFile.swift
//  DTB
//
//  Created by Five Exceptions on 08/09/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import Foundation
import  UIKit

class Session {
    
    static let sharedInstance = Session()
    var bottamSpace : CGFloat?
    var isAuthnicated : Bool?
    var cupJSON : JSON?
    var beJSON : JSON?
    var upcomingBillsJSON : JSON?
    var fixedDepoJSON : JSON?
    var fdMessage : String?
    var isPrint = false
    var isUpdatedBEInfo = false
    var isFDCalling = true
    var isFirstTimeAPiCall = false
    var contentOffset : CGPoint?
    var beAPICallTime : Date?
    var firstTimeAccoutAlise : String?
    var lockCardtime : Date?
    var cardInfo : [[String : Any]]?
    let timerManager = TimerManager()
    var mainBEAPICalling : Bool?
    var aliseWhenMainAPICalling = [String]()
}
