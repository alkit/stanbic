//
//  UITextField+Extension.swift
//  DTB
//
//  Created by AlkitGupta on 25/09/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import UIKit

extension UITextField{
    
    
    
    //shake
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.4
        animation.values = [-15.0, 15.0, -15.0, 15.0, -9.0, 9.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    // add error label
    func showError(errStr : String){
        self.shake()
        self.resignFirstResponder()
        for subview in self.subviews{ //remove error label
            if let lineView = subview as? UIImageView{
                lineView.frame.size.height = 2
                lineView.backgroundColor = .red
            }
        }
        let x = 0 //Int(-1 * (self.frame.minX - 20))
        let y = Int(self.frame.height + CGFloat(2))
        let wid = self.frame.maxX - 20
        let label = UILabel(frame: CGRect(x: x, y: y, width: Int(wid), height: 14))
        label.numberOfLines = 0
        label.text = errStr
        label.font = UIFont.init(name: Fonts.kFontMedium, size: CGFloat(10))
        label.textColor = .red
        self.addSubview(label)
    }
    
    
    //remove error from textfield when not editing
    func removeError(){
        for subview in self.subviews{ //remove error label
            if let label = (subview as? UILabel){
                if label.center.y > self.bounds.midY{
                    label.removeFromSuperview()
                }
            }else if let lineView = subview as? UIImageView{
                lineView.frame.size.height = 1
                lineView.backgroundColor = .gray
            }
        }
        self.resignFirstResponder()
    }
            
           
    func addUI(placeholder:String){
        DispatchQueue.main.async{
            self.addLine()
            self.addLabll(placeholder: placeholder)
        }
    }
    
    func didBegin(){
        for subview in self.subviews{  //remove error label
            
            if let lineView = subview as? UIImageView{
                lineView.frame.size.height = 2
                lineView.backgroundColor = UIColor.blue
            
            }else if let label = (subview as? UILabel){
                
                    if label.transform == .identity{
                        label.alpha = 0
                        label.textColor = .lightGray
                        
                        UIView.animate(withDuration: 0.2, delay: 0.15, options: .transitionCrossDissolve, animations: {
                            label.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
                            label.frame = CGRect(x: 0, y: Int(self.bounds.minY - 12), width: Int(label.frame.width), height: 18)
                            label.textColor = UIColor.blue
                            label.alpha = 1
                        }, completion: nil)
                    }
            }
        }
        
    }
    
    func didEnd(){
        if self.text == ""{
            for subview in self.subviews{
                if let lineView = subview as? UIImageView{
                    lineView.frame.size.height = 0
                    lineView.backgroundColor = .lightGray
                
                }else if let label = subview as? UILabel{
                    
                    UIView.animate(withDuration: 0.2, animations: {
                        label.transform = CGAffineTransform.identity
                        label.frame = CGRect(x: 0, y: 0, width: Int(label.frame.width), height: 18)
                        label.center.y = self.bounds.midY
                        label.textColor = UIColor.lightGray
                        
                    }) { (true) in
                        
                    }
                }
            }
        }else{
            for subview in self.subviews{  //remove error label
                if let lineView = subview as? UIImageView{
                    lineView.frame.size.height = 0
                    lineView.backgroundColor = .lightGray
                }
            }
        }
    }
    
    private func addLabll(placeholder:String){
        // Add placeholder
        var wid = self.frame.width
        let placeLbl = UILabel()
        placeLbl.textColor = UIColor.lightGray
        placeLbl.text = placeholder
        let contentWidth = placeLbl.intrinsicContentSize.width
        wid = contentWidth < wid ? contentWidth : wid
        placeLbl.frame = CGRect(x: 0, y: 0, width: Int(wid) + 4, height: 18)
        placeLbl.center.y = self.bounds.midY
        placeLbl.backgroundColor = UIColor.white
        self.addSubview(placeLbl)
    }
    
    private func addLine(){
        // Add line
        let x = -13
        let y = Int(self.frame.height)
        let widt = self.frame.width + 26
        let line = UIImageView(frame: CGRect(x: x, y: y, width: Int(widt), height: 0))
        self.addSubview(line)
    }

}//END

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}
