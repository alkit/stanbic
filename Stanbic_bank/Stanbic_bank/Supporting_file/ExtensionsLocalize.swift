//
//  Extensions.swift
//  DTB
//
//  Created by Sumit5Exceptions on 05/09/18.
//  Copyright © 2018 Five Exceptions. All rights reserved.
//

import Foundation

//ConvertingAppLanaguage
extension String
{
    
    func localized(_ lang:String) -> String
    {
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
    }
    
}
