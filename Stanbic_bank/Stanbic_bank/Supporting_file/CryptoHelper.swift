//
//  CryptoHelper.swift
//  LionsAttendance
//
//  Created by Mata Prasad Chauhan on 01/11/17.
//

import Foundation
import CryptoSwift

class CryptoHelper {
    
   // public static var key = "ukh0bjd7cszhy0na";//16 char secret key //ukh0bjd7cszhy0na

    public static func encrypt(input:String)->String? {
        do{

            let value = Authntication().loadPasswordFromKeychainAndAuthenticateUser2(KeychainConfiguration.encptValue, service: KeychainConfiguration.encptKey)
            
            //value = decrypt(input: value)!
            
            
            let encrypted: Array<UInt8> = try AES(key: value, iv: value, padding: .pkcs5).encrypt(Array(input.utf8))
            
            return encrypted.toBase64()
        }catch{
            
        }
        return nil
    }
    
    public static func decrypt(input:String)->String? {
        do{
            
            let value = Authntication().loadPasswordFromKeychainAndAuthenticateUser2(KeychainConfiguration.encptValue, service: KeychainConfiguration.encptKey)
            
            //value = decrypt(input: value)!
            let d=Data(base64Encoded: input)
            let decrypted = try AES(key: value, iv: value, padding: .pkcs5).decrypt(
                d!.bytes)
            return String(data: Data(decrypted), encoding: .utf8)
        }catch{
            
        }
        return ""
    }
}
